package com.rol.calendar.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.rol.calendar.settings.SubscribeRoomResponse;
import com.rol.calendar.vo.Integration;
import com.rol.calendar.vo.settings.SettingResponse;
import com.rol.calendar.vo.settings.SharedResource;
import com.rol.calendar.vo.settings.SubscribeRoomBean;
import com.rol.calendar.vo.settings.UnSubscribeRoomBean;

/**
 * @author Anup
 *
 */
public interface SubscriptionClient {

	/**
	 * Gets the setting response from ROL servers
	 * 
	 * @return
	 */
	CompletableFuture<List<SettingResponse>> getRolSettings();

	/**
	 * Gets the setting response from ROL servers
	 * 
	 * @return
	 */
	CompletableFuture<List<SettingResponse>> getDbSettings();

	/**
	 * @param integration
	 */
	void addCompany(Integration integration);

	/**
	 * @param integration
	 */
	void deleteCompany(Long companyId);

	/**
	 * @param zone
	 */
	void addRoom(SharedResource zone);

	/**
	 * @param roldId
	 */
	void deleteRoom(Long roldId);
	
	/**
	 * @param subsJson
	 */
	void updateRoom(String subsJson);
	
	/**
	 * Send the request to Gsuite calendar service for subscription of room
	 * @return
	 */
	SubscribeRoomResponse subscribeRoom(SubscribeRoomBean subsRoomBean) ;
	
	/**
	 * Send the request to Gsuite calendar service to un-subscribe room
	 */
	void unsubscribeRoom(UnSubscribeRoomBean unSubsRoomBean) ;
	
	/**
	 * @param zid
	 * @return
	 */
	SharedResource getRoom(String zid) ;
	
	/**
	 * @param subsRoomResp
	 */
	void genRoomSyncToken(SubscribeRoomResponse subsRoomResp) ;

}

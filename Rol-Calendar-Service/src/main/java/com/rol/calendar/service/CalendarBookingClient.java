package com.rol.calendar.service;

import java.util.concurrent.CompletableFuture;

/**
 * ROL Calendar Client for CRUD operation with ROL calendar REST API
 * 
 * @author Anup
 *
 */
public interface CalendarBookingClient {

	/**
	 * Create Booking
	 * 
	 * @param <T>
	 * @param resourceURL
	 * @param element
	 * @return RequestResult
	 */
	public <T> CompletableFuture<String> createBooking(final T element);

	/**
	 * Get Booking details using the Booking Id
	 * 
	 * @param urlSuffix
	 * @return RequestResult
	 */
	public CompletableFuture<String> getBookingById(String bookingId);

	/**
	 * Update Booking by Booking Id
	 * 
	 * @param <T>
	 * @param element
	 * @return RequestResult
	 */
	public <T> CompletableFuture<String> updateBooking(final T element,String bookingId);

	/**
	 * Delete Booking by Booking Id
	 * 
	 * @param bookingId
	 * @return RequestResult
	 */
	public CompletableFuture<String> deleteBookingById(String bookingId);

}

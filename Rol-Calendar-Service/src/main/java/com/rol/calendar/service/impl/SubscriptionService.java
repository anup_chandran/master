package com.rol.calendar.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.rol.calendar.service.SubscriptionClient;
import com.rol.calendar.settings.SubscribeRoomResponse;
import com.rol.calendar.util.HeaderUtil;
import com.rol.calendar.vo.Integration;
import com.rol.calendar.vo.settings.SettingResponse;
import com.rol.calendar.vo.settings.SharedResource;
import com.rol.calendar.vo.settings.SubscribeRoomBean;
import com.rol.calendar.vo.settings.UnSubscribeRoomBean;

/**
 * @author Anup
 *
 */
@Component("SubscriptionServiceRest")
public class SubscriptionService implements SubscriptionClient {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private HeaderUtil httpHeader;

	@Value("${rol_settings_url}")
	private String settingsUrl;

	@Value("${database_service_base}")
	private String databaseServiceBaseUrl;

	@Value("${database_service_companies}")
	private String databaseServiceCompanies;

	@Value("${database_service_del_room}")
	private String urlDelRoom;

	@Value("${database_service_company}")
	private String urlCompany;

	@Value("${database_service_create_room}")
	private String urlCreateRoom;
	
	@Value("${gsuite_subscribe_room}")
	private String subscribeRoomUrl ;
	
	@Value("${gsuite_unsubscribe_room}")
	private String unSubscribeRoomUrl ;
	

	@Override
	public CompletableFuture<List<SettingResponse>> getRolSettings() {

		ResponseEntity<SettingResponse[]> response = restTemplate.exchange(settingsUrl, HttpMethod.GET,
				httpHeader.getEntityMap(), SettingResponse[].class);

		return CompletableFuture.completedFuture(Arrays.asList(response.getBody()));
	}

	@Override
	public CompletableFuture<List<SettingResponse>> getDbSettings() {
		ResponseEntity<SettingResponse[]> response = restTemplate
				.getForEntity(databaseServiceBaseUrl.concat(databaseServiceCompanies), SettingResponse[].class);

		return CompletableFuture.completedFuture(Arrays.asList(response.getBody()));
	}

	@Override
	public void deleteRoom(Long roldId) {
		restTemplate.delete(databaseServiceBaseUrl.concat(urlDelRoom).concat(String.valueOf(roldId)));
	}

	@Override
	public void addCompany(Integration integration) {
		ResponseEntity<String> respEntity = restTemplate.postForEntity(databaseServiceBaseUrl.concat(urlCompany),
				integration, String.class);
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not Add company in DB for  ".concat(String.valueOf(integration.getCid())));
		}
	}

	@Override
	public void addRoom(SharedResource zone) {
		ResponseEntity<String> respEntity = restTemplate.postForEntity(databaseServiceBaseUrl.concat(urlCreateRoom),
				zone, String.class);
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not Add Room in DB for ".concat(String.valueOf(zone.getGcalResourceEmail())));
		}

	}

	@Override
	public void deleteCompany(Long companyId) {
		restTemplate.delete(databaseServiceBaseUrl.concat(urlCompany).concat(String.valueOf("/" + companyId)));
	}

	@Override
	public void updateRoom(String subsJson) {
		restTemplate.put(databaseServiceBaseUrl.concat(urlCreateRoom), subsJson);
	}

	@Override
	public SubscribeRoomResponse subscribeRoom(SubscribeRoomBean subsRoomBean) {
		
		ResponseEntity<SubscribeRoomResponse> respEntity = restTemplate.postForEntity(subscribeRoomUrl, subsRoomBean, SubscribeRoomResponse.class) ;
		
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not subscribe for ".concat(String.valueOf(subsRoomBean.getGresourceid())));
		}
		
		return  respEntity.getBody();
	}

	public void unsubscribeRoom(UnSubscribeRoomBean unSubsRoom) {
		ResponseEntity<String> respEntity = restTemplate.postForEntity(unSubscribeRoomUrl, unSubsRoom, String.class) ;
		
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not subscribe for ".concat(String.valueOf(unSubsRoom.getResourceId())));
		}
	}

	@Override
	public SharedResource getRoom(String zid) {
		
		ResponseEntity<SharedResource> respEntity = restTemplate.getForEntity(databaseServiceBaseUrl.concat(urlDelRoom).concat(zid), SharedResource.class) ;
		
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not get Room Details for ".concat(zid));
		}
		
		return respEntity.getBody() ;
	}
	
	public void genRoomSyncToken(SubscribeRoomResponse subsRoomResp) {
		ResponseEntity<String> respEntity = restTemplate.postForEntity("http://localhost:7000/gcal/room/synctoken", subsRoomResp, String.class) ;
		
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not subscribe for ".concat(String.valueOf(subsRoomResp.getResourceId())));
		}
	}

}

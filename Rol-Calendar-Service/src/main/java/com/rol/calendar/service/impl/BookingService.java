package com.rol.calendar.service.impl;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.rol.calendar.service.CalendarBookingClient;
import com.rol.calendar.util.HeaderUtil;

/**
 * @author Anup
 *
 */
@Service("BookingService")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BookingService implements CalendarBookingClient {
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${rol_api_url}")
	private String rolBaseUrlBooking;
	
	@Autowired
	private ObjectFactory<HeaderUtil> httpHeaderFac;

	private static final Logger logger = LoggerFactory.getLogger(BookingService.class) ;

	@Override
	@Async
	public <T> CompletableFuture<String> createBooking(T element) {

//		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		ResponseEntity<String> response = restTemplate.postForEntity(rolBaseUrlBooking, httpHeaderFac.getObject().getEntityMap(element), String.class);

		return CompletableFuture.completedFuture(response.getBody());
	}

	@Override
	@Async
	public CompletableFuture<String> getBookingById(String bookingId) {

		ResponseEntity<String> response = restTemplate.exchange(getRolUrl(bookingId), HttpMethod.GET,
				httpHeaderFac.getObject().getEntityMap(), String.class);

		return CompletableFuture.completedFuture(response.getBody());
	}

	@Override
	@Async
	public <T> CompletableFuture<String> updateBooking(T element,String bookingId) {
		logger.info("Intializing the update request for {}",bookingId);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(rolBaseUrlBooking.concat("/").concat(bookingId), HttpMethod.PUT,
				httpHeaderFac.getObject().getEntityMap(element), String.class);

		return CompletableFuture.completedFuture(response.getBody());
	}

	@Override
	@Async
	public CompletableFuture<String> deleteBookingById(String bookingId) {

		final String accessUrl = getRolUrl(bookingId);
		ResponseEntity<String> respEntity = restTemplate.exchange(accessUrl, HttpMethod.DELETE,
				httpHeaderFac.getObject().getEntityMap(), String.class);

		return CompletableFuture.completedFuture(respEntity.getBody());
	}

	/**
	 * @param bookingId
	 * @return
	 */
	private String getRolUrl(final String bookingId) {
		return rolBaseUrlBooking.concat("/").concat(bookingId);
	}

}

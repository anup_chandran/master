package com.rol.calendar.config;

import java.util.Collections;

import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import interceptors.RestClientInterceptor;	

@Component("RolRestTemplateConf")
public class RolRestTemplateConf implements RestTemplateCustomizer {

	@Override
	public void customize(RestTemplate restTemplate) {
		
		HttpComponentsClientHttpRequestFactory clientReqFactory = new HttpComponentsClientHttpRequestFactory() ;
		
		clientReqFactory.setReadTimeout(500000);
		clientReqFactory.setConnectTimeout(500000);
		
		 ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(clientReqFactory); 
		 restTemplate.setRequestFactory(factory);
		 
		restTemplate.setInterceptors(Collections.singletonList(new RestClientInterceptor()));
		

	}

}

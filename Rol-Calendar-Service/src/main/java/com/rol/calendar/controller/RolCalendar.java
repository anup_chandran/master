package com.rol.calendar.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rol.calendar.service.CalendarBookingClient;
import com.rol.calendar.vo.CreateVo;

/**
 * @author Anup
 *
 */
@RestController
public class RolCalendar {
	
	@Autowired
	@Qualifier("BookingService")
	private ObjectFactory<CalendarBookingClient> calendarClientFac ;
	
	Logger logger = LoggerFactory.getLogger(RolCalendar.class) ;
	/**
	 * @param createVo
	 * @return
	 */
	@PostMapping(value = "/booking",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String bookingById(@RequestBody CreateVo createVo) {
		return calendarClientFac.getObject().createBooking(createVo).join();
	}
	
	/**
	 * @param bookingId
	 * @return
	 */
	@GetMapping(value = "/booking/{bookingId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String bookingById(@PathVariable Long bookingId) {
		return calendarClientFac.getObject().getBookingById(String.valueOf(bookingId)).join() ;
	}
	
	/**
	 * @param createVo
	 * @param bookingId
	 * @return
	 */
	@PutMapping(value="/booking/{bookingId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateBookingById(@RequestBody CreateVo createVo,@PathVariable Long bookingId) {
		String result = calendarClientFac.getObject().updateBooking(createVo,String.valueOf(bookingId)).join() ;
		logger.info("Completed Request for {}",bookingId);
		return  result;
	}
	
	/**
	 * @param createVo
	 * @param bookingId
	 * @return
	 */
	@DeleteMapping("/booking/{bookingId}")
	public String deleteBookingById(@PathVariable String bookingId) {
		return calendarClientFac.getObject().deleteBookingById(bookingId).join() ;
	}

}

package com.rol.calendar.vo.settings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"system",
"username",
"defaultuser"
})
public class Properties {

@JsonProperty("system")
private String system;
@JsonProperty("username")
private String username;
@JsonProperty("defaultuser")
private String defaultuser;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<>();

@JsonProperty("system")
public String getSystem() {
return system;
}

@JsonProperty("system")
public void setSystem(String system) {
this.system = system;
}

@JsonProperty("username")
public String getUsername() {
return username;
}

@JsonProperty("username")
public void setUsername(String username) {
this.username = username;
}

@JsonProperty("defaultuser")
public String getDefaultuser() {
return defaultuser;
}

@JsonProperty("defaultuser")
public void setDefaultuser(String defaultuser) {
this.defaultuser = defaultuser;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
package com.rol.calendar.vo.settings;

import java.util.Objects;

/**
 * @author Anup
 *
 */
public class SettingsOperation {
	
	/**
	 * @param integration
	 * @return
	 */
	public boolean isGsuite(Integration integration) {
		return (Objects.nonNull(integration)
				&& "google".equals(integration.getProperties().getSystem())
				&& "ESC".equals(integration.getType()));
	}
	

}

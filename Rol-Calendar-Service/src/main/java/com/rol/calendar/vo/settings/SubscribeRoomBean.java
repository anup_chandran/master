package com.rol.calendar.vo.settings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "gresourceid", "defaultuser" })
public class SubscribeRoomBean {

	@JsonProperty("gresourceid")
	private String gresourceid;
	@JsonProperty("defaultuser")
	private String defaultuser;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();
	
	public SubscribeRoomBean() {}
	
	public SubscribeRoomBean(String defaultUsr,String resourceMailId) {
		this.gresourceid = resourceMailId ;
		this.defaultuser = defaultUsr ;
	}

	@JsonProperty("gresourceid")
	public String getGresourceid() {
		return gresourceid;
	}

	@JsonProperty("gresourceid")
	public void setGresourceid(String gresourceid) {
		this.gresourceid = gresourceid;
	}

	@JsonProperty("defaultuser")
	public String getDefaultuser() {
		return defaultuser;
	}

	@JsonProperty("defaultuser")
	public void setDefaultuser(String defaultuser) {
		this.defaultuser = defaultuser;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

package com.rol.calendar.vo.settings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Cid", "Realm", "Created", "LastModified", "Integration" })
public class SettingResponse {

	@JsonProperty("Cid")
	private Long cid;
	@JsonProperty("Realm")
	private Long realm;
	@JsonProperty("Created")
	private Long created;
	@JsonProperty("LastModified")
	private Long lastModified;
	@JsonProperty("Integration")
	private Integration integration;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("Cid")
	public Long getCid() {
		return cid;
	}

	@JsonProperty("Cid")
	public void setCid(Long cid) {
		this.cid = cid;
	}

	@JsonProperty("Realm")
	public Long getRealm() {
		return realm;
	}

	@JsonProperty("Realm")
	public void setRealm(Long realm) {
		this.realm = realm;
	}

	@JsonProperty("Created")
	public Long getCreated() {
		return created;
	}

	@JsonProperty("Created")
	public void setCreated(Long created) {
		this.created = created;
	}

	@JsonProperty("LastModified")
	public Long getLastModified() {
		return lastModified;
	}

	@JsonProperty("LastModified")
	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	@JsonProperty("Integration")
	public Integration getIntegration() {
		return integration;
	}

	@JsonProperty("Integration")
	public void setIntegration(Integration integration) {
		this.integration = integration;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "SettingResponse [cid=" + cid + ", realm=" + realm + ", created=" + created + ", lastModified="
				+ lastModified + ", integration=" + integration + ", additionalProperties=" + additionalProperties
				+ "]";
	}

}
package com.rol.calendar.vo.settings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Zid", "ExternalId", "Cid" })
public class SharedZone {

	@JsonProperty("Zid")
	private Long zid;
	@JsonProperty("ExternalId")
	private String externalId;
	@JsonProperty("Cid")
	private Long cid;
	@JsonProperty("Tz")
	private String timeZone ;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("Zid")
	public Long getZid() {
		return zid;
	}

	@JsonProperty("Zid")
	public void setZid(Long zid) {
		this.zid = zid;
	}

	@JsonProperty("ExternalId")
	public String getExternalId() {
		return externalId;
	}

	@JsonProperty("ExternalId")
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@JsonProperty("Cid")
	public Long getCid() {
		return cid;
	}

	@JsonProperty("Cid")
	public void setCid(Long cid) {
		this.cid = cid;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
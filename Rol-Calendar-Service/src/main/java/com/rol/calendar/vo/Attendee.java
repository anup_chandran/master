package com.rol.calendar.vo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Username", "Required", "Status", "Guests" })
public class Attendee {

	@JsonProperty("Username")
	private String username;
	@JsonProperty("Required")
	private Boolean required;
	@JsonProperty("Status")
	private String status;
	@JsonProperty("Guests")
	private Integer guests;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("Username")
	public String getUsername() {
		return username;
	}

	@JsonProperty("Username")
	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("Required")
	public Boolean getRequired() {
		return required;
	}

	@JsonProperty("Required")
	public void setRequired(Boolean required) {
		this.required = required;
	}

	@JsonProperty("Status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("Status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("Guests")
	public Integer getGuests() {
		return guests;
	}

	@JsonProperty("Guests")
	public void setGuests(Integer guests) {
		this.guests = guests;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

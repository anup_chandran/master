package com.rol.calendar.vo.settings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Active", "Type", "Properties", "SharedZones" })
public class Integration {

	@JsonProperty("Active")
	private Boolean active;
	@JsonProperty("Type")
	private String type;
	@JsonProperty("Properties")
	private Properties properties;
	@JsonProperty("SharedZones")
	private List<SharedZone> sharedZones = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("Active")
	public Boolean getActive() {
		return active;
	}

	@JsonProperty("Active")
	public void setActive(Boolean active) {
		this.active = active;
	}

	@JsonProperty("Type")
	public String getType() {
		return type;
	}

	@JsonProperty("Type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("Properties")
	public Properties getProperties() {
		return properties;
	}

	@JsonProperty("Properties")
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	@JsonProperty("SharedZones")
	public List<SharedZone> getSharedZones() {
		return sharedZones;
	}

	@JsonProperty("SharedZones")
	public void setSharedZones(List<SharedZone> sharedZones) {
		this.sharedZones = sharedZones;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
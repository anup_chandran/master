package com.rol.calendar.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Owner", "ExternalId", "LastModified", "From", "Until", "Zid", "Subject", "Desc", "Source",
		"Properties", "Attendees", "Private" })
public class CreateVo {

	@JsonProperty("Owner")
	private String owner;
	@JsonProperty("ExternalId")
	private String externalId;
	@JsonProperty("LastModified")
	private Long lastModified;
	@JsonProperty("From")
	private Long from;
	@JsonProperty("Until")
	private Long until;
	@JsonProperty("Zid")
	private Long zid;
	@JsonProperty("Subject")
	private String subject;
	@JsonProperty("Desc")
	private Object desc;
	@JsonProperty("Source")
	private String source;
	@JsonProperty("Properties")
	private Properties properties;
	@JsonProperty("Attendees")
	private List<Attendee> attendees = null;
	@JsonProperty("Private")
	private Boolean privateMeeting;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("Owner")
	public String getOwner() {
		return owner;
	}

	@JsonProperty("Owner")
	public void setOwner(String owner) {
		this.owner = owner;
	}

	@JsonProperty("ExternalId")
	public String getExternalId() {
		return externalId;
	}

	@JsonProperty("ExternalId")
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@JsonProperty("LastModified")
	public Long getLastModified() {
		return lastModified;
	}

	@JsonProperty("LastModified")
	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	@JsonProperty("From")
	public Long getFrom() {
		return from;
	}

	@JsonProperty("From")
	public void setFrom(Long from) {
		this.from = from;
	}

	@JsonProperty("Until")
	public Long getUntil() {
		return until;
	}

	@JsonProperty("Until")
	public void setUntil(Long until) {
		this.until = until;
	}

	@JsonProperty("Zid")
	public Long getZid() {
		return zid;
	}

	@JsonProperty("Zid")
	public void setZid(Long zid) {
		this.zid = zid;
	}

	@JsonProperty("Subject")
	public String getSubject() {
		return subject;
	}

	@JsonProperty("Subject")
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@JsonProperty("Desc")
	public Object getDesc() {
		return desc;
	}

	@JsonProperty("Desc")
	public void setDesc(Object desc) {
		this.desc = desc;
	}

	@JsonProperty("Source")
	public String getSource() {
		return source;
	}

	@JsonProperty("Source")
	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("Properties")
	public Properties getProperties() {
		return properties;
	}

	@JsonProperty("Properties")
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	@JsonProperty("Attendees")
	public List<Attendee> getAttendees() {
		return attendees;
	}

	@JsonProperty("Attendees")
	public void setAttendees(List<Attendee> attendees) {
		this.attendees = attendees;
	}

	@JsonProperty("Private")
	public Boolean getPrivate() {
		return privateMeeting;
	}

	@JsonProperty("Private")
	public void setPrivate(Boolean privateMeeting) {
		this.privateMeeting = privateMeeting;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

package com.rol.calendar.vo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

/**
 * @author anup_r
 *
 */
public class RolCalResp {
	
	private static final Logger logger = LoggerFactory.getLogger(RolCalResp.class) ;
			
	private  String responseEntity="";
	private int clientCode ;
	private boolean error ;
	private String accessUrl ;
	
	
	
	public RolCalResp(final ResponseEntity<String> response){
		responseEntity = response.getBody();
		clientCode= response.getStatusCodeValue();
	}
	
	public RolCalResp(final String respStr,final int statusCode){
		responseEntity = respStr;
		clientCode= statusCode;
	}
	
	public RolCalResp(final String respStr,final int statusCode,final boolean error){
		responseEntity = respStr;
		clientCode= statusCode;
		this.error = error ;
	}
	
	public RolCalResp(){
		
	}
	
	
	public void logErrorMessage(){
		if(getClientCode() == 400){
			//possibility of having the meeting already registered 
			logger.error("Bad request , possibility of having the meeting already registered.");
		}else {
			logger.error("Check the status code and log..");
		}
	}

	
	public String getResponseEntity() {
		return responseEntity;
	}

	public int getClientCode() {
		return clientCode;
	}
	
	

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getAccessUrl() {
		return accessUrl;
	}

	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

}

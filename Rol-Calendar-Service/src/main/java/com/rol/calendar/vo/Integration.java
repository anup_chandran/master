package com.rol.calendar.vo;

import com.rol.calendar.vo.settings.SettingResponse;

public class Integration {

	private Long integrationId;

	private Long cid;

	private Long realm;

	private Long lastModified;

	private String active;

	private String defaultUser;
	
	public Integration() {
	}
	public Integration(SettingResponse sr) {
		setIntegrationId(sr.getCid());
		setCid(sr.getCid());
		setRealm(sr.getRealm());
		setLastModified(sr.getLastModified());
		setActive(sr.getIntegration().getActive().toString());
		setDefaultUser(sr.getIntegration().getProperties().getDefaultuser());
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public Long getRealm() {
		return realm;
	}

	public void setRealm(Long realm) {
		this.realm = realm;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getDefaultUser() {
		return defaultUser;
	}

	public void setDefaultUser(String defaultUser) {
		this.defaultUser = defaultUser;
	}

	public Long getIntegrationId() {
		return integrationId;
	}

	public void setIntegrationId(Long integrationId) {
		this.integrationId = integrationId;
	}

}

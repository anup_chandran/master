package com.rol.calendar.vo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"esync-email",
"esync-name",
"event-id"
})
public class Properties {
	@JsonProperty("esync-email")
	private String esyncEmail;
	@JsonProperty("esync-name")
	private String esyncName;
	@JsonProperty("event-id")
	private String eventId;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("esync-email")
	public String getEsyncEmail() {
	return esyncEmail;
	}

	@JsonProperty("esync-email")
	public void setEsyncEmail(String esyncEmail) {
	this.esyncEmail = esyncEmail;
	}

	@JsonProperty("esync-name")
	public String getEsyncName() {
	return esyncName;
	}

	@JsonProperty("esync-name")
	public void setEsyncName(String esyncName) {
	this.esyncName = esyncName;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	@JsonProperty("event-id")
	public String getEventId() {
		return eventId;
	}
	@JsonProperty("event-id")
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
}

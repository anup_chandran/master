package com.rol.calendar.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Component
@Scope("prototype")
public class HeaderUtil {
	
	@Value("${rol_auth_user_key}")
	private String rolAuthUserKey;
	@Value("${rol_auth_user}")
	private String rolAuthUser;

	@Value("${rol_auth_user_password_key}")
	private String rolAuthPasswordKey;
	@Value("${rol_auth_password}")
	private String rolAuthPassword;
	
	
	/**
	 * @return
	 */
	private void addConstants(MultiValueMap<String, String> headers) {
		headers.add("Content-Type", "application/vnd.idesk-v5+json");
		headers.add("Accept", "application/vnd.idesk-v5+json");
		headers.add("idesk-auth-method", "up");
	}
	
	/**
	 * @return
	 */
	public HttpEntity<String> getEntityMap() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		addConstants(headers);

		headers.add(rolAuthUserKey, rolAuthUser);
		headers.add(rolAuthPasswordKey, rolAuthPassword);
		

		return new HttpEntity<>(headers);

	}
	public <T> HttpEntity<T> getEntityMap(T element) {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		addConstants(headers);
		
		headers.add(rolAuthUserKey, rolAuthUser);
		headers.add(rolAuthPasswordKey, rolAuthPassword);

		return new HttpEntity<>(element,headers);

	}

}

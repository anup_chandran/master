package com.rol.calendar.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import com.rol.calendar.vo.TokenErrorBean;


/**
 * @author Anup
 *
 */
@ControllerAdvice
@Component
public class ExceptionAdvise {
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionAdvise.class.getSimpleName());
	private static final HttpStatus httpStatus = HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()) ; 
	
	private static final String APP_VERSION = "1.0" ;
	
	
	/**
	 * Method to handle Exception , un-handled Exception
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> exceptionHandler(Exception ex) {
		TokenErrorBean errorBean = new TokenErrorBean();
		errorBean.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
		errorBean.setAppVersion(APP_VERSION);
		errorBean.setErrorMessage(ex.getMessage());
		
		logger.error("UnCaught Exception !!! ", ex);
		
		return new ResponseEntity<>(errorBean,httpStatus);
	}
	
	
	/**
	 * Method to handle missing parameter  Exception
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<Object> exceptionHandler(MissingServletRequestParameterException ex) {
		TokenErrorBean errorBean = new TokenErrorBean();
		errorBean.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST));
		errorBean.setAppVersion(APP_VERSION);
		errorBean.setErrorMessage(ex.getMessage());
		
		return new ResponseEntity<>(errorBean,httpStatus);
	}
	/**
	 * Method to handle AuthorizeTokenException
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(HttpClientErrorException.class)
	public ResponseEntity<Object> exceptionHandler(HttpClientErrorException ex) {
		TokenErrorBean errorBean = new TokenErrorBean();
		errorBean.setErrorCode(ex.getStatusText());
		errorBean.setAppVersion(APP_VERSION);
		errorBean.setErrorMessage(ex.getMessage());
		
		logger.error("UnCaught Exception !!! ", ex);
		
		return new ResponseEntity<>(errorBean,httpStatus);
	}
	
	

}

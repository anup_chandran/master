package com.rol.calendar.settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.rol.calendar.service.SubscriptionClient;
import com.rol.calendar.vo.settings.Integration;
import com.rol.calendar.vo.settings.SettingResponse;
import com.rol.calendar.vo.settings.SharedResource;
import com.rol.calendar.vo.settings.SharedZone;
import com.rol.calendar.vo.settings.SubscribeRoomBean;
import com.rol.calendar.vo.settings.UnSubscribeRoomBean;

/**
 * @author Anup Runnable class to execute the sync operation , Singleton class
 *         with check to run one instance
 */
@Component
public class SettingsProcessorBean implements Runnable {

	private boolean processing;

	@Autowired
	@Qualifier("SubscriptionServiceRest")
	private SubscriptionClient client;

	private List<SettingResponse> integrationCache = new ArrayList<>();
	private List<SettingResponse> rolSettingsResp = new ArrayList<>();

	private List<SettingResponse> activeComp = new ArrayList<>();
	private List<SettingResponse> inactiveComp = new ArrayList<>();

	@Override
	public void run() {

		if (processing) {// Do not process as integration from previous call is still running
			return;
		} else {
			processing = true;// Lock it for processing
		}

		try {
			integrationCache.addAll(client.getDbSettings().join());// Get the integration Database
			rolSettingsResp.addAll(client.getRolSettings().join());// Get the settings from ROL Server

			filterGoogle();
			segregateEnabledDisabled();
			processDisabledCompanies();
			processEnabledNewCompanies();

		} finally {
			integrationCache.clear();
			rolSettingsResp.clear();
			activeComp.clear();
			inactiveComp.clear();
			processing = false;
		}

	}

	/**
	 * 
	 */
	private void processEnabledNewCompanies() {
		for (SettingResponse sr : activeComp) {
			// Check if it is available in database
			if (!checkIntegrationDb(sr)) {
				// Add the integration
				client.addCompany(new com.rol.calendar.vo.Integration(sr));
				// Each of the room must be Subscribed, Add to db
				for (SharedZone zone : sr.getIntegration().getSharedZones()) {
					processSubscription(zone,sr);
				}
			}

		}
	}
	
	/**
	 * @param zone
	 * @param sr
	 */
	private void processSubscription(SharedZone zone,SettingResponse sr) {
		SubscribeRoomBean subRmBn = new SubscribeRoomBean(sr.getIntegration().getProperties().getDefaultuser(),zone.getExternalId()) ;
		// Subscribe
		SubscribeRoomResponse subsRoom = client.subscribeRoom(subRmBn);
		// save the information in database
		client.addRoom(new SharedResource(zone, subsRoom));
		//Initiate for sync token
		client.genRoomSyncToken(subsRoom);
	}
	

	/**
	 * Removes all the disabled companies if present
	 */
	private void processDisabledCompanies() {
		for (SettingResponse in : inactiveComp) {

			List<SharedZone> lstShardZone = checkIntegrationDbDel(in);// Shared-zone will be empty when company is
																		// disabled
			// Check if it is available in database
			if (!lstShardZone.isEmpty()) {
				// Each of the room must be un-subscribed, remove from db
				for (SharedZone room : lstShardZone) {
					//We will need the resourceId which we got back during subscription
					SharedResource shrdRes = client.getRoom(String.valueOf(room.getZid())) ;
					
					UnSubscribeRoomBean unSubsRmBean = new UnSubscribeRoomBean() ;
					unSubsRmBean.setDefaultuser(in.getIntegration().getProperties().getDefaultuser());
					unSubsRmBean.setId(shrdRes.getGchannelId());
					unSubsRmBean.setResourceId(shrdRes.getGresourceId());
					
					client.unsubscribeRoom(unSubsRmBean);
					client.deleteRoom(room.getZid());
				}
			}

			if (checkIntegrationDb(in)) {
				// Remove the Company
				client.deleteCompany(in.getCid());
			}
		}
	}

	/**
	 * @return
	 */
	private boolean checkIntegrationDb(SettingResponse settResp) {

		for (SettingResponse settRespCache : integrationCache) {
			if (settRespCache.getCid().longValue() == settResp.getCid().longValue()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return
	 */
	private List<SharedZone> checkIntegrationDbDel(SettingResponse settRespDisabled) {

		for (SettingResponse settRespCache : integrationCache) {
			if (settRespCache.getCid().longValue() == settRespDisabled.getCid().longValue()) {
				return settRespCache.getIntegration().getSharedZones();
			}
		}

		return Collections.emptyList();
	}

	/**
	 * 
	 */
	private void segregateEnabledDisabled() {
		Integration in = null;

		for (SettingResponse comp : rolSettingsResp) {
			in = comp.getIntegration();
			if (in.getActive().booleanValue()) {// Check if it exists in db
				activeComp.add(comp);
			} else {// Check if it is disabled
				inactiveComp.add(comp);
			}
		}

	}

	/**
	 * Filters out the non google companies
	 */
	private void filterGoogle() {
		Iterator<SettingResponse> itrSettingResp = rolSettingsResp.iterator();
		SettingResponse setResp = null;
		while (itrSettingResp.hasNext()) {
			setResp = itrSettingResp.next();
			if (!isGsuite(setResp.getIntegration())) {
				itrSettingResp.remove();
			}
		}

	}

	/**
	 * @param integration
	 * @return
	 */
	private boolean isGsuite(Integration integration) {
		return (Objects.nonNull(integration) && "google".equals(integration.getProperties().getSystem())
				&& "ESC".equals(integration.getType()));
	}

	public boolean isProcessing() {
		return processing;
	}

}

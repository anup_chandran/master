package com.rol.calendar.settings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "kind", "id", "resourceId", "resourceUri", "token", "expiration" })
public class SubscribeRoomResponse {

	@JsonProperty("kind")
	private String kind;
	@JsonProperty("id")
	private String id;
	@JsonProperty("resourceId")
	private String resourceId;
	@JsonProperty("resourceUri")
	private String resourceUri;
	@JsonProperty("token")
	private String token;
	@JsonProperty("expiration")
	private String expiration;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("kind")
	public String getKind() {
		return kind;
	}

	@JsonProperty("kind")
	public void setKind(String kind) {
		this.kind = kind;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("resourceId")
	public String getResourceId() {
		return resourceId;
	}

	@JsonProperty("resourceId")
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@JsonProperty("resourceUri")
	public String getResourceUri() {
		return resourceUri;
	}

	@JsonProperty("resourceUri")
	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}

	@JsonProperty("token")
	public String getToken() {
		return token;
	}

	@JsonProperty("token")
	public void setToken(String token) {
		this.token = token;
	}

	@JsonProperty("expiration")
	public String getExpiration() {
		return expiration;
	}

	@JsonProperty("expiration")
	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
package com.rol.calendar.settings;

import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author Anup
 * 
 *         Extracts the information from the setting URL of the ROL Server and
 *         process
 *
 */
@Component
public class SettingsProcessor {

	Logger logger = LoggerFactory.getLogger(SettingsProcessor.class);
	@Autowired
	private SettingsProcessorBean processBean;
	@Autowired
	@Qualifier("SingleThreadSync")
	private ExecutorService executorService ;

	/**
	 * Triggered as configured in the property file for integration of the settings from ROL
	 */
	@Scheduled(fixedDelayString = "${rol_setting_delay}")
	public void process() {
		if(!processBean.isProcessing()) {
			executorService.execute(processBean) ;
		}
	}

}

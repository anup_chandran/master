package com.rol.notification.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Configuration
public class PubSubPublisher {

	private static final Logger LOGGER = LoggerFactory.getLogger(PubSubPublisher.class);
	
	@Value("${pubsub_topic_notification_name}")
	private String topicName ;
	
	private static final String CHANNEL_NAME = "pubSubOutputChannel" ;

	@Bean
	@ServiceActivator(inputChannel = CHANNEL_NAME)
	public MessageHandler messageSender(PubSubTemplate pubsubTemplate) {
		
		
		PubSubMessageHandler adapter = new PubSubMessageHandler(pubsubTemplate, topicName);
		adapter.setPublishCallback(new ListenableFutureCallback<String>() {
			@Override
			public void onFailure(Throwable ex) {
				LOGGER.error("There was an error sending the message to Topic {}",adapter.getTopicExpression().getValue().toString(),ex);
			}

			@Override
			public void onSuccess(String result) {
				LOGGER.info("Message was sent successfully.".concat(result));
			}
		});

		return adapter;
	}
	
	/**
	 * interface for sending a message to Pub/Sub.
	 */
	@MessagingGateway(defaultRequestChannel = CHANNEL_NAME)
	public interface PubSubOutboundGateway {
		void sendToPubSub(String text);
	}
}

package com.rol.notification.vo;

public class NotificationVo {

	private String contentType;// : application/json; utf-8
	private String channelID;// : 4ba78bf0-6a47-11e2-bcfd-0800200c9a66
	private String channel;// -Token: 398348u3tu83ut8uu38
	private String channelExpiration;// : Tue, 19 Nov 2013 01:13:52 GMT
	private String resourceId; // : ret08u3rv24htgh289g
	private String resourceUri; // :
								// https://www.googleapis.com/calendar/v3/calendars/my_calendar@gmail.com/events
	private String resourceState;// : exists
	private String messageNumber;// : 10

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getChannelID() {
		return channelID;
	}

	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelExpiration() {
		return channelExpiration;
	}

	public void setChannelExpiration(String channelExpiration) {
		this.channelExpiration = channelExpiration;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceUri() {
		return resourceUri;
	}

	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}

	public String getResourceState() {
		return resourceState;
	}

	public void setResourceState(String resourceState) {
		this.resourceState = resourceState;
	}

	public String getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(String messageNumber) {
		this.messageNumber = messageNumber;
	}

}

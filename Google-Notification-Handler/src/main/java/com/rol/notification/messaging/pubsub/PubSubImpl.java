package com.rol.notification.messaging.pubsub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rol.notification.configuration.PubSubPublisher.PubSubOutboundGateway;
import com.rol.notification.messaging.PublishMessage;

/**
 * @author Anup
 *
 */
@Component("PubSub")
public class PubSubImpl implements PublishMessage {
	
	@Autowired
	private PubSubOutboundGateway pubSubGateway;

	/**
	 * Send the message to the Topic 
	 */
	@Override
	public void send(String message) {
		pubSubGateway.sendToPubSub(message);
	}

}

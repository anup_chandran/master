package com.rol.notification.messaging;

public interface PublishMessage {
	
	public void send(final String message) ;

}

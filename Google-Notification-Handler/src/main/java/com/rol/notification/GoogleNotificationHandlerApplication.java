package com.rol.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GoogleNotificationHandlerApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(GoogleNotificationHandlerApplication.class, args);
	}

}

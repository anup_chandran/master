package com.rol.notification.logcontext;

/**
 * @author Anup
 *
 */
public enum MdcKeyValue {
	
	//Keys 
	
	LOGMSG("trace"),
	
	/*
	 * COLLUSIONDETECTIONENABLED("YES"), COLLUSIONDETECTIONDISABLED("YES"),
	 */
	NOTIFICATION("Notification") ;
	 
	//For Type of Notification
	private String desc ;
		
	private MdcKeyValue(String desc) {
		this.desc = desc;
	}
	private MdcKeyValue() {
		this.desc = "";
	}
	public String getDesc() {
		return desc;
	}
	
}

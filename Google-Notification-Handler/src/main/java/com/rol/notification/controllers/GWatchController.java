package com.rol.notification.controllers;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.rol.notification.logcontext.MdcKeyValue;
import com.rol.notification.logcontext.MdcUtil;
import com.rol.notification.messaging.PublishMessage;

/**
 * @author Anup
 *
 */
@RestController
public class GWatchController {

	private static final Logger logger = LoggerFactory.getLogger(GWatchController.class);

	private static final String[] WATCH_HEADERS = { "X-Goog-Channel-ID", "X-Goog-Channel-Token",
			"X-Goog-Channel-Expiration", "X-Goog-Resource-ID", "X-Goog-Resource-URI", "X-Goog-Resource-State",
			"X-Goog-Message-Number" };

	@Autowired
	@Qualifier("PubSub")
	private PublishMessage publishMsg;
	@Autowired
	private Gson gson;

	@PostMapping(value = "/process")
	public void registerNotification(HttpServletResponse response, HttpServletRequest request) throws IOException {

		try {
			String jsonReq = getJsonToQueue(request);
			if(StringUtils.isNoneEmpty(jsonReq)) {
				logger.info("Sending the request !");
				publishMsg.send(jsonReq);
			}
			MdcUtil.addSingleEntry(MdcKeyValue.NOTIFICATION.name(), jsonReq);
			logger.info("Completed the Request !");


		} finally {
			// Clear MDC after executing the request
			MdcUtil.clearMdc();
		}
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		response.setContentType(MediaType.TEXT_HTML_VALUE);
	}

	/**
	 * Format captured from the google watch format X-Goog-Channel-ID:
	 * directoryApiId X-Goog-Channel-Token: 398348u3tu83ut8uu38
	 * X-Goog-Channel-Expiration: Tue, 29 Oct 2013 20:32:02 GMT X-Goog-Resource-ID:
	 * ret08u3rv24htgh289g X-Goog-Resource-URI:
	 * 'https://www.googleapis.com/admin/directory/v1/users?domain=domain&event=event
	 * X-Goog-Resource-State: event X-Goog-Message-Number: 10 { "kind":
	 * "admin#directory#user", "id": long, "etag": string, "primaryEmail": string }
	 * 
	 * @return
	 * @throws IOException
	 */
	private String getJsonToQueue(HttpServletRequest request) throws IOException {
		final String notificationDetails = new String(StreamUtils.copyToByteArray(request.getInputStream()));
		
		logger.info(notificationDetails);
		
		Map<String, String> bodyVars = new HashMap<>() ;
		
		Map<String, String> bodyVarsResult = gson.fromJson(notificationDetails, Map.class);
		
		if(Objects.nonNull(bodyVarsResult)) {
			bodyVars.putAll(bodyVarsResult); 
		}
		
		Enumeration<String> headerNames = request.getHeaderNames() ;
		
		if(logger.isDebugEnabled()) {
			while(headerNames.hasMoreElements()) {
				logger.debug(headerNames.nextElement());
				logger.debug(request.getHeader(headerNames.nextElement()));
			}
		}
		

		for (int wt = 0; wt < WATCH_HEADERS.length; wt++) {
			logger.info("wt {} watchHeaders {}",wt,WATCH_HEADERS[wt]);
			bodyVars.put(WATCH_HEADERS[wt], getHeaderValue(request, WATCH_HEADERS[wt]));
		}
		
		return gson.toJson(bodyVars) ;

	}

	private String getHeaderValue(HttpServletRequest request, String headerName) {
		return request.getHeader(headerName);
	}

}

package com.rol.notification.exception.custom;

/**
 * @author Anup
 *  Custom exception , base
 *
 */
public class RolGCalSyncException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1217026581445748942L;
	private final String errMsg;

	public RolGCalSyncException(String errorMsg) {
		this.errMsg = errorMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}

}

package com.rol.notification.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


/**
 * @author Anup
 *
 */
@ControllerAdvice
@Component
public class ExceptionAdvise {
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionAdvise.class.getSimpleName());
	
	
	
	/**
	 * Method to handle Exception , un-handled Exception
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(Exception ex) {
		
		logger.error("UnCaught Exception !!! ", ex);
		
	}
	
	
	
	
	

}

package com.rol.notification.exception.custom;

/**
 * @author Anup
 *  Custom exception , base
 *
 */
public class ParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1217026581445748942L;
	private final String errMsg;

	public ParseException(String errorMsg) {
		this.errMsg = errorMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}

}

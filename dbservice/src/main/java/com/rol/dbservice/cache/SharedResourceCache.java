package com.rol.dbservice.cache;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.map.LRUMap;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.rol.dbservice.dao.SharedResource;

/**
 * @author Anup
 *
 * Simple cache for Shared Resource From db,
 *  Max allowed of 1000 entries , 
 *  
 */
public class SharedResourceCache {
	private  Map<String, SharedResource> sharedResCache ;
	@Autowired
	private Gson gson ;
	
	
	public SharedResourceCache() {
		setLruMap() ;
	}
	
	private void setLruMap() {
		//Setting the Max size to be 5000
		sharedResCache = new LRUMap<>(1000);
		//Make the Map Thread Safe
		sharedResCache = Collections.synchronizedMap(sharedResCache);
	}
	
	/**
	 * Cleans up map every 12 hours
	 */
	public void resetMap() {
		sharedResCache.clear();
	}
	/**
	 * Update the Cache
	 * @param userName
	 * @throws RolGCalSyncException
	 */
	public void updateCache(final String userName, final SharedResource authTknBean) {
		
		sharedResCache.put(userName, authTknBean);
		
	}
	
	public void removeCache(final String key) {
		sharedResCache.remove(key);
	}
	
	public boolean isKeyPresent(final String userName) {
		return Objects.nonNull(sharedResCache.get(userName)) ;
	}
	
	public SharedResource getFromCache(final String key) {
		return sharedResCache.get(key) ;
	}
	
	/**
	 * Adds the token if not present
	 */
	public void addRoomCache(final String userName,final SharedResource authTknBean) {
		updateCache(userName,authTknBean) ;
	}
	
	public String findAll() {
		return gson.toJson(sharedResCache.values()) ;
	}

}

CREATE DATABASE IF NOT EXISTS gscal; 
use gscal;

create TABLE IF NOT EXISTS `gscal`.`sharedresources` (
    `shared_resource_id` MEDIUMINT NOT NULL AUTO_INCREMENT,
    `rol_resource_id` VARCHAR(255),
    `gcal_resource_email` VARCHAR(255),
    `gcal_resource_name` varchar(255),           
    `time_zone` varchar(255),
    `integration_id` mediumint(9),
    PRIMARY KEY (shared_resource_id)
)ENGINE = InnoDB;

create TABLE IF NOT EXISTS `gscal`.`integration` (
    `integrationid` mediumint NOT NULL AUTO_INCREMENT,
	`cid` bigint,
	`realm` bigint,
	`lastmodified` bigint,
	`active` varchar(10),
	`defaultuser` varchar(100),
	PRIMARY KEY(integrationid)
 )ENGINE = InnoDB;
 
 
  create TABLE IF NOT EXISTS `gscal`.`gscalbooking`
	(
		`gscalid` MEDIUMINT NOT NULL AUTO_INCREMENT,
		`rolbookingid` VARCHAR(255),
		`rollastmodified` bigint,
		`gcalusericaluid` text,
		`gcalusereventid` text,
        `gcalresourceeventid` text,
		`gcallastmodified` bigint,		
		`sharedresourceid` bigint NOT NULL,
		`gcaluseremail` VARCHAR(255),
		`gcalusername` VARCHAR(255),
		`roluseremail` VARCHAR(255),
		`isrecurrence` CHAR(6),
		PRIMARY KEY(gscalid)
)ENGINE = InnoDB;


create table IF NOT EXISTS `gscal`.`gscalmetadata` (
	`metadataid` mediumint NOT NULL AUTO_INCREMENT,
    `metakey` varchar(255),
    `metavalue` varchar(255),
	PRIMARY KEY(metadataid)
)ENGINE = InnoDB;

create index `gscalbooking_index` on `gscal`.`gscalbookinginfo`(rolbooking_id);

create index `sharedresources_index on `gscal`.`sharedresources`(gcal_resource_email);

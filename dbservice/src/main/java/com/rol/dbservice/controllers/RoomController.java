package com.rol.dbservice.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rol.dbservice.dao.SharedResource;
import com.rol.dbservice.dao.operation.DbBasicOperations;

@RestController
public class RoomController {
	
	@Autowired
	@Qualifier("Rooms")
	private DbBasicOperations<SharedResource> roomDetailsProvider ;
	
	
	@GetMapping(value = "/rooms/{integrationId}",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SharedResource> getAllRoomByIntegrationId(@PathVariable String integrationId) {
		return roomDetailsProvider.findAll(integrationId) ;
	}
	/**
	 * @param roomId
	 * @return
	 */
	@GetMapping(value = "/room/gmail/{roomId}",produces = MediaType.APPLICATION_JSON_VALUE)
	public SharedResource getRoomById(@PathVariable String roomId) {
		return roomDetailsProvider.findById(roomId) ;
	}
	/**
	 * @param roomId
	 * @return
	 */
	@GetMapping(value = "/room/rolid/{roomId}",produces = MediaType.APPLICATION_JSON_VALUE)
	public SharedResource getRoomByRolId(@PathVariable String roomId) {
		return roomDetailsProvider.findByRolId(roomId) ;
	}
	
	/**
	 * @param room
	 * @param response
	 */
	@PostMapping(value = "/room",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveRoom(@RequestBody SharedResource room,HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		roomDetailsProvider.save(room) ;
	}
	/**
	 * @param room
	 * @param response
	 */
	@PutMapping(value = "/room",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateRoom(@RequestBody SharedResource room,HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		roomDetailsProvider.update(room) ;
	}
	/**
	 * @param roomId
	 * @param response
	 */
	@DeleteMapping(value = "/room/rolid/{roomId}",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteRoom(@PathVariable String roomId,HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		roomDetailsProvider.delete(roomId);
	}

}

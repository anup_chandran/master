package com.rol.dbservice.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.rol.dbservice.dao.MetaData;
import com.rol.dbservice.dao.impl.MetaDataImpl;

/**
 * @author EI11388
 *
 */
/**
 *
 */
@RestController
public class MetaDataController {
	
	@Autowired
	@Qualifier("rolmetadata")
	private MetaDataImpl metaDataDetailsProvider ;
	
	/**
	 * @param bookingId
	 * @return
	 */
	@GetMapping(value = "/metadata",produces = MediaType.APPLICATION_JSON_VALUE)
	public MetaData getMetaData(@PathVariable(required = false) String metadatakey ) {
		return metaDataDetailsProvider.findMetaData() ;
	}

	
	
}

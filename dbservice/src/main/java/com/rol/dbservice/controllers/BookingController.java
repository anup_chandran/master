package com.rol.dbservice.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rol.dbservice.dao.Booking;
import com.rol.dbservice.dao.SharedResource;
import com.rol.dbservice.dao.operation.DbBasicOperations;

@RestController
public class BookingController {

	@Autowired
	@Qualifier("roombooking")
	private DbBasicOperations<Booking> bookingDetailsProvider;

	/**
	 * @param bookingId
	 * @return
	 */
	@GetMapping(value = "/booking/bookingid/{bookingId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Booking getBookingById(@PathVariable String bookingId) {
		return bookingDetailsProvider.findById(bookingId);
	}

	/**
	 * @param booking
	 * @param response
	 */

	@PostMapping(value = "/booking", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveBooking(@RequestBody Booking booking, HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		bookingDetailsProvider.save(booking);
	}

	/**
	 * @param booking
	 * @param response
	 */
	@PutMapping(value = "/booking", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateBooking(@RequestBody Booking booking, HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		bookingDetailsProvider.update(booking);
	}

	/**
	 * @param bookingId
	 * @param response
	 */
	@DeleteMapping(value = "/booking/bookingid/{bookingId}")
	public void deleteBooking(@PathVariable String bookingId, HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		bookingDetailsProvider.delete(bookingId);
	}

}

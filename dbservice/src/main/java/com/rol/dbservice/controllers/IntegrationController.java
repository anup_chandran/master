package com.rol.dbservice.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rol.dbservice.dao.Integration;
import com.rol.dbservice.dao.operation.DbBasicOperations;

/**
 *
 */
@RestController
public class IntegrationController {

	@Autowired
	@Qualifier("rolintegration")
	private DbBasicOperations<Integration> integrationDetailsProvider;

	/**
	 * @return
	 */
	@GetMapping(value = "/integrations", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Integration> getAllIntegrations() {
		return integrationDetailsProvider.findAll();
	}

	/**
	 * @param roomId
	 * @return
	 */
	@GetMapping(value = "/integration/{integrationid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Integration getIntegrationById(@PathVariable String integrationid) {
		return integrationDetailsProvider.findById(integrationid);
	}

	/**
	 * @param room
	 * @param response
	 */
	@PostMapping(value = "/integration", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveIntegration(@RequestBody Integration integration, HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		integrationDetailsProvider.save(integration);
	}

	/**
	 * @param room
	 * @param response
	 */
	@PutMapping(value = "/integration", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateUpdateIntegration(@RequestBody Integration integration, HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		integrationDetailsProvider.update(integration);
	}

	/**
	 * @param roomId
	 * @param response
	 */
	@DeleteMapping(value = "/integration/{integrationid}")
	public void deleteDeleteIntegration(@PathVariable String integrationid, HttpServletResponse response) {
		response.setStatus(HttpStatus.NO_CONTENT.value());
		integrationDetailsProvider.delete(integrationid);
	}

}

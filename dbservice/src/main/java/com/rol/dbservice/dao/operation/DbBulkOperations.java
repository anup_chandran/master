package com.rol.dbservice.dao.operation;

import java.util.List;

public interface DbBulkOperations<T> {
	/**
	 * @param <T>
	 * @param objs
	 * @return
	 */
	public boolean save(List<T> objs) ;
	
	/**
	 * @param <T>
	 * @param objs
	 * @return
	 */
	public boolean update(List<T> objs) ;
	
	/**
	 * @param <T>
	 * @param objs
	 * @return
	 */
	public boolean delete(List<T> objs) ;
	
	
}

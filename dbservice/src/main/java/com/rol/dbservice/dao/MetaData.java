package com.rol.dbservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MetaData {

	private Integer metadataid;

	private String metadatakey;

	private String metadatavalue;

	public MetaData() {

	}

	
	public MetaData(ResultSet rs, int rowNum) throws SQLException {
	
		setMetadataid(rs.getInt("metadataid")) ;
		setMetadatakey(rs.getString("metakey"));
		setMetadatavalue(rs.getString("metavalue")) ;
	
		
	}

	public Integer getMetadataid() {
		return metadataid;
	}

	public void setMetadataid(Integer metadataid) {
		this.metadataid = metadataid;
	}

	public String getMetadatakey() {
		return metadatakey;
	}

	public void setMetadatakey(String metadatakey) {
		this.metadatakey = metadatakey;
	}

	public String getMetadatavalue() {
		return metadatavalue;
	}

	public void setMetadatavalue(String metadatavalue) {
		this.metadatavalue = metadatavalue;
	}

}

package com.rol.dbservice.dao;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class ObtainBeanInstance implements ApplicationContextAware{
	
	private ApplicationContext context = null;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext ;
	}
	
	public <T> T getBean(Class beanName) {
		return (T) context.getBean(beanName);
	}

}

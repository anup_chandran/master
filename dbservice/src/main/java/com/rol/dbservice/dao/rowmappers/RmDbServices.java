package com.rol.dbservice.dao.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rol.dbservice.dao.Booking;
import com.rol.dbservice.dao.Integration;
import com.rol.dbservice.dao.MetaData;
import com.rol.dbservice.dao.SharedResource;

/**
 * Services class with multiple RowMapper implementation
 * 
 * @author Anup
 *
 */
public class RmDbServices {

	/**
	 * RowMapper Implementation for SharedResource
	 */
	public class SharedResourceRm implements RowMapper<SharedResource> {

		@Override
		public SharedResource mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new SharedResource(rs, rowNum);
		}

	}

	/**
	 * RowMapper Implementation for Integration
	 */
	public class IntegrationRm implements RowMapper<Integration> {

		@Override
		public Integration mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Integration(rs, rowNum);
		}

	}

	/**
	 * RowMapper Implementation for Booking
	 *
	 */
	public class BookingRm implements RowMapper<Booking> {

		@Override
		public Booking mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Booking(rs, rowNum);
		}
	}

	/**
	 * RowMapper Implementation for MetaData
	 *
	 */
	public class MetaDataRm implements RowMapper<MetaData> {
		@Override
		public MetaData mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new MetaData(rs, rowNum);
		}
	}

}

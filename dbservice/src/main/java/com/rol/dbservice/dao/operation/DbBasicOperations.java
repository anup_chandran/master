package com.rol.dbservice.dao.operation;

import java.util.List;

/**
 * @author Anup
 *
 */
public interface DbBasicOperations<T> {

	/**
	 * @param <T>
	 * @param T
	 * @return
	 */
	public T findById(String gMailId);
	
	/**
	 * @param gMailId
	 * @return
	 */
	public default T findByRolId(String gMailId) {
		return null ;
	}

	/**
	 * @param <T>
	 * @param objs
	 * @return
	 */
	public boolean save(T obj);

	/**
	 * @param <T>
	 * @param objs
	 * @return
	 */
	public boolean update(T obj);

	/**
	 * @param <T>
	 * @param obj
	 * @return
	 */
	public boolean delete(String gMailId);
	
	/**
	 * @param integrationId
	 * @return
	 */
	public default List<T> findAll(final String integrationId){
		return null ;
	}
	
	/**
	 * @param integrationId
	 * @return
	 */
	public default List<T> findAll(){
		return null ;
	}

}

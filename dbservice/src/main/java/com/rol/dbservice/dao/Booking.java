package com.rol.dbservice.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Booking {
	
	private int gsCalId;

	private Long rolBookingId;

	private Long rolLastModified;

	private String gscalUserICalUId;

	private String gscalUserEventId;

	private String gscalResourceEventId;

	private Long gscalLastModified;

	private Long sharedResourceId;

	private String gscalUserEmail;

	private String gscalUserName;

	private String rolUserEmail;

	private String isRecurrence;
	
	public Booking()
	{
		
	}
	
	public Booking(ResultSet rs, int rowNum) throws SQLException {
			
			setGsCalId(rs.getInt("gscalid"));
			setRolBookingId(rs.getLong("rolbookingid"));
			setRolLastModified(rs.getLong("rollastmodified"));
			setGscalUserICalUId(rs.getString("gcalusericaluid"));
			setGscalUserEventId(rs.getString("gcalusereventid"));
			setGscalResourceEventId(rs.getString("gcalresourceeventid"));
			setGscalLastModified(rs.getLong("gcallastmodified"));
			setSharedResourceId(rs.getLong("sharedresourceid"));
			setGscalUserEmail(rs.getString("gcaluseremail"));
			setGscalUserName(rs.getString("gcalusername"));
			setRolUserEmail(rs.getString("roluseremail"));
			setIsRecurrence(rs.getString("isrecurrence"));
			
		}


	public int getGsCalId() {
		return gsCalId;
	}


	public void setGsCalId(int gsCalInfoId) {
		this.gsCalId = gsCalInfoId;
	}


	public Long getRolBookingId() {
		return rolBookingId;
	}


	public void setRolBookingId(Long rolBookingId) {
		this.rolBookingId = rolBookingId;
	}


	public Long getRolLastModified() {
		return rolLastModified;
	}


	public void setRolLastModified(Long rolLastModified) {
		this.rolLastModified = rolLastModified;
	}


	public String getGscalUserICalUId() {
		return gscalUserICalUId;
	}


	public void setGscalUserICalUId(String gscalUserICalUId) {
		this.gscalUserICalUId = gscalUserICalUId;
	}


	public String getGscalUserEventId() {
		return gscalUserEventId;
	}


	public void setGscalUserEventId(String gscalUserEventId) {
		this.gscalUserEventId = gscalUserEventId;
	}


	public String getGscalResourceEventId() {
		return gscalResourceEventId;
	}


	public void setGscalResourceEventId(String gscalResourceEventId) {
		this.gscalResourceEventId = gscalResourceEventId;
	}


	public Long getGscalLastModified() {
		return gscalLastModified;
	}


	public void setGscalLastModified(Long gscalLastModified) {
		this.gscalLastModified = gscalLastModified;
	}


	public Long getSharedResourceId() {
		return sharedResourceId;
	}


	public void setSharedResourceId(Long sharedResourceId) {
		this.sharedResourceId = sharedResourceId;
	}


	public String getGscalUserEmail() {
		return gscalUserEmail;
	}


	public void setGscalUserEmail(String gscalUserEmail) {
		this.gscalUserEmail = gscalUserEmail;
	}


	public String getGscalUserName() {
		return gscalUserName;
	}


	public void setGscalUserName(String gscalUserName) {
		this.gscalUserName = gscalUserName;
	}


	public String getRolUserEmail() {
		return rolUserEmail;
	}


	public void setRolUserEmail(String rolUserEmail) {
		this.rolUserEmail = rolUserEmail;
	}


	public String getIsRecurrence() {
		return isRecurrence;
	}


	public void setIsRecurrence(String isRecurrence) {
		this.isRecurrence = isRecurrence;
	}

}

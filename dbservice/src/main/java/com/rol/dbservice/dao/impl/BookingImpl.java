package com.rol.dbservice.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.rol.dbservice.dao.Booking;
import com.rol.dbservice.dao.operation.DbBasicOperations;
import com.rol.dbservice.dao.rowmappers.RmDbServices;

@Component("roombooking")
public class BookingImpl implements DbBasicOperations<Booking> {

	@Autowired
	private NamedParameterJdbcTemplate template;

	@Override
	public Booking findById(String gscalId) {
		final String query = "SELECT * FROM gscalbooking WHERE gscalid = :gscalid";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("gscalid", gscalId);
		RmDbServices.BookingRm innerObj = new RmDbServices().new BookingRm();
		Booking bookingDet = null;
		List<Booking> bookingDetail = template.query(query, namedParameters, innerObj);
		Optional<Booking> booking = bookingDetail.stream().findFirst();
		if (booking.isPresent()) {
			bookingDet = booking.get();
		}
		return bookingDet;
	}

	@Override
	public boolean save(Booking booking) {

		final String query = "INSERT INTO gscalbooking "
				+ "(rolbookingid,rollastmodified,gcalusericaluid,gcalusereventid,sharedresourceid,gcalresourceeventid,isrecurrence,gcallastmodified,gcaluseremail,gcalusername,roluseremail) "
				+ "VALUES "
				+ "(:rolbookingId,:rolLastModified,:gscalUserICalUId,:gscalUserEventId,:sharedResourceId,:gscalResEvtId,:isRecurrence,:gscalLastModified,:gscalUserEmail,:gscalUserName,:rolUserEmail)";

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("rolbookingId", booking.getRolBookingId());
		namedParameters.addValue("rolLastModified", booking.getRolLastModified());
		namedParameters.addValue("gscalUserICalUId", booking.getGscalUserICalUId());
		namedParameters.addValue("gscalUserEventId", booking.getGscalUserEventId());
		namedParameters.addValue("sharedResourceId", booking.getSharedResourceId());
		namedParameters.addValue("gscalResEvtId", booking.getGscalResourceEventId());
		namedParameters.addValue("isRecurrence", booking.getIsRecurrence());
		namedParameters.addValue("gscalLastModified", booking.getGscalLastModified());
		namedParameters.addValue("gscalUserEmail", booking.getGscalUserEmail());
		namedParameters.addValue("gscalUserName", booking.getGscalUserName());
		namedParameters.addValue("rolUserEmail", booking.getRolUserEmail());
		template.update(query, namedParameters);
		return true;
	}

	@Override
	public boolean update(Booking booking) {
		final String query = "update gscalbooking SET "
				+ " rolbookingid = :rolbookingId,rollastmodified = :rolLastModified,gcalusericaluid = :gsCalUserICalUId"
				+ " ,gcalusereventid = :gsCalUserEventId,sharedresourceid= :sharedResourceId,gcalresourceeventid= :gsCalResEvtId ,"
				+ "isrecurrence=  :isRecurrence,gcallastmodified= :gsCalLastModified  ,gcaluseremail= :gsCalUserEmail ,gcalusername= :gsCalUserName , roluseremail= :rolUserEmail"
				+ " where gscalid = :gscalId ;";

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("gscalId", booking.getGsCalId());
		namedParameters.addValue("rolbookingId", booking.getRolBookingId());
		namedParameters.addValue("rolLastModified", booking.getRolLastModified());
		namedParameters.addValue("gsCalUserICalUId", booking.getGscalUserICalUId());
		namedParameters.addValue("gsCalUserEventId", booking.getGscalUserEventId());
		namedParameters.addValue("sharedResourceId", booking.getSharedResourceId());
		namedParameters.addValue("gsCalResEvtId", booking.getGscalResourceEventId());
		namedParameters.addValue("isRecurrence", booking.getIsRecurrence());
		namedParameters.addValue("gsCalLastModified", booking.getGscalLastModified());
		namedParameters.addValue("gsCalUserEmail", booking.getGscalUserEmail());
		namedParameters.addValue("gsCalUserName", booking.getGscalUserName());
		namedParameters.addValue("rolUserEmail", booking.getRolUserEmail());
		template.update(query, namedParameters);
		return true;
	}

	@Override
	public boolean delete(String gscalId) {
		
		final String query = "delete from gscalbooking where  gscalid = :gscalId" ;
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("gscalId",gscalId);	
		template.update(query, namedParameters);
		
		return true;
		
	}

}

package com.rol.dbservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SharedResource {

	public SharedResource() {
	}

	public SharedResource(ResultSet rs, int rowNum) throws SQLException {

		setRolResourceId(rs.getString("rolid"));
		setGcalResourceEmail(rs.getString("gcalemail"));
		setTimeZone(rs.getString("timezone"));
		setIntegrationId(rs.getInt("integrationid"));

		setGchannelId(rs.getString("gchannelid"));
		setGresourceId(rs.getString("gresourceid"));
		setGresourceUri(rs.getString("gresourceuri"));
		setGexpiration(rs.getString("gexpiration"));
		setGsynctoken(rs.getString("gsynctoken"));
	}

	private String rolid;
	private String gcalemail;
	private String timezone;
	private int integrationid;

	private String gchannelId;
	private String gresourceId;
	private String gresourceUri;
	private String gexpiration;

	private String gsynctoken;

	public String getRolResourceId() {
		return rolid;
	}

	public void setRolResourceId(String rolResourceId) {
		this.rolid = rolResourceId;
	}

	public String getTimeZone() {
		return timezone;
	}

	public void setTimeZone(String timeZone) {
		this.timezone = timeZone;
	}

	public int getIntegrationId() {
		return integrationid;
	}

	public void setIntegrationId(int integrationId) {
		this.integrationid = integrationId;
	}

	public String getGcalResourceEmail() {
		return gcalemail;
	}

	public void setGcalResourceEmail(String gcalResourceEmail) {
		this.gcalemail = gcalResourceEmail;
	}


	public void setRolid(String rolid) {
		this.rolid = rolid;
	}


	public void setGcalemail(String gcalemail) {
		this.gcalemail = gcalemail;
	}


	public String getGchannelId() {
		return gchannelId;
	}

	public void setGchannelId(String gchannelId) {
		this.gchannelId = gchannelId;
	}

	public String getGresourceId() {
		return gresourceId;
	}

	public void setGresourceId(String gresourceId) {
		this.gresourceId = gresourceId;
	}

	public String getGresourceUri() {
		return gresourceUri;
	}

	public void setGresourceUri(String gresourceUti) {
		this.gresourceUri = gresourceUti;
	}

	public String getGexpiration() {
		return gexpiration;
	}

	public void setGexpiration(String gexpiration) {
		this.gexpiration = gexpiration;
	}

	public String getGsynctoken() {
		return gsynctoken;
	}

	public void setGsynctoken(String gsynctoken) {
		this.gsynctoken = gsynctoken;
	}

}

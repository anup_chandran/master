package com.rol.dbservice.dao.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.rol.dbservice.cache.SharedResourceCache;
import com.rol.dbservice.dao.SharedResource;
import com.rol.dbservice.dao.operation.DbBasicOperations;
import com.rol.dbservice.dao.rowmappers.RmDbServices;

@Component("Rooms")
public class SharedResourceImpl implements DbBasicOperations<SharedResource> {

	@Autowired
	private NamedParameterJdbcTemplate template;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private SharedResourceCache roomCache;

	/**
	 * The notification will have the gresourceid - Google ResourceId
	 */
	@Override
	public SharedResource findById(String id) {
		// Get from Cache
		if (Objects.nonNull(roomCache.getFromCache(id))) {
			return roomCache.getFromCache(id);
		}

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("gresourceid", id);

		RmDbServices.SharedResourceRm innerObj = new RmDbServices().new SharedResourceRm();

		List<SharedResource> roomList = template.query(Queries.ROOM_BY_GRESOURCEID, namedParameters, innerObj);
		SharedResource room = null;

		Optional<SharedResource> optShrd = roomList.stream().findFirst();
		// Update cache before returning
		if (optShrd.isPresent()) {
			room = optShrd.get();
			roomCache.addRoomCache(room.getGcalResourceEmail(), room);
		}

		return room;
	}

	/**
	 *
	 */
	@Override
	public boolean save(SharedResource room) {
		final String query = "INSERT INTO sharedresource (rolid, gcalemail, timezone, integrationid, gchannelid, gresourceid, gresourceuri, gexpiration, gsynctoken) "
				+ "VALUES (?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(query, room.getRolResourceId(), room.getGcalResourceEmail(), room.getTimeZone(),
				room.getIntegrationId(), room.getGchannelId(), room.getGresourceId(), room.getGresourceUri(),
				room.getGexpiration(), room.getGsynctoken(), room.getIntegrationId(), room.getTimeZone());
		return true;
	}

	/**
	 *
	 */
	@Override
	public boolean update(SharedResource room) {
		final String query = "update sharedresource SET "
				+ " gcalemail = :gcalResourceEmail,gcalname = :gcalResourceName,timezone = :timeZone ,integrationid = :integrationId"
				+ " where rolid = :rolResourceId ;";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("rolResourceId", room.getRolResourceId());
		namedParameters.addValue("timeZone", room.getTimeZone());
		namedParameters.addValue("integrationId", room.getIntegrationId());

		template.update(query, namedParameters);

		if (Objects.nonNull(roomCache.getFromCache(room.getGcalResourceEmail()))) {
			roomCache.addRoomCache(room.getGcalResourceEmail(), room);
		}

		return true;
	}

	/**
	 * Delete the room
	 */
	@Override
	public boolean delete(String rolid) {
		final String query = "delete from sharedresource where rolid = :rolResourceId";

		final SharedResource room = findByRolId(rolid);

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("rolResourceId", rolid);
		template.update(query, namedParameters);

		roomCache.removeCache(room.getGcalResourceEmail());

		return true;
	}

	/**
	 *
	 */
	@Override
	public SharedResource findByRolId(String rolid) {

		final String query = "SELECT * FROM sharedresource WHERE rolid = :rolResourceId";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("rolResourceId", rolid);

		RmDbServices.SharedResourceRm innerObj = new RmDbServices().new SharedResourceRm();

		List<SharedResource> roomList = template.query(query, namedParameters, innerObj);
		SharedResource room = null;

		Optional<SharedResource> optShrd = roomList.stream().findFirst();
		// Update cache before returning
		if (optShrd.isPresent()) {
			room = optShrd.get();
		}

		return room;
	}

	@Override
	public List<SharedResource> findAll(String integrationId) {
		final String query = "SELECT * FROM sharedresource WHERE integrationid = :integrationId";

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("integrationId", integrationId);

		RmDbServices.SharedResourceRm innerObj = new RmDbServices().new SharedResourceRm();

		return template.query(query, namedParameters, innerObj);
	}

}

package com.rol.dbservice.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.rol.dbservice.dao.Integration;
import com.rol.dbservice.dao.operation.DbBasicOperations;
import com.rol.dbservice.dao.rowmappers.RmDbServices;

@Component("rolintegration")
public class IntegrationImpl implements DbBasicOperations<Integration> {

	@Autowired
	private NamedParameterJdbcTemplate template;

	/**
	 *
	 */
	@Override
	public boolean save(Integration integration) {

		final String query = "INSERT INTO integration " + "(cid,realm,lastmodified,active,defaultuser) " + "VALUES "
				+ "(:cid,:realm,:lastModified,:active,:defaultUser)";

		template.update(query, getSaveNamedParam(integration));

		return true;
	}

	/**
	 *
	 */
	@Override
	public boolean update(Integration integration) {
		final String query = "update integration SET " + " cid = :cid,realm = :realm,lastmodified = :lastModified "
				+ " ,active = :active,defaultuser= :defaultUser" + " where integrationid = :integrationId ;";

		template.update(query, getUpdateNamedParam(integration));
		
		return true;
	}

	/**
	 *
	 */
	@Override
	public boolean delete(String integrationId) {

		final String query = "delete from integration where  integrationid = :integrationId";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("integrationId", integrationId);
		template.update(query, namedParameters);

		return true;
	}

	/**
	 *
	 */
	@Override
	public Integration findById(String integrationId) {
		final String query = " select * from integration where integrationid = :integrationId  ";
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		
		RmDbServices.IntegrationRm innerObj = new RmDbServices().new IntegrationRm();
		namedParameters.addValue("integrationId", integrationId);
		
		List<Integration> result = template.query(query, namedParameters, innerObj);
		
		Integration integration = null;
		Optional<Integration> findFirst = result.stream().findFirst();
		if (findFirst.isPresent()) {
			integration = findFirst.get();
		}
		
		return integration;
	}
	
	/**
	 *
	 */
	@Override
	public List<Integration> findAll(){
		final String query = " select * from integration  ";
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		
		RmDbServices.IntegrationRm innerObj = new RmDbServices().new IntegrationRm();
		
		return template.query(query, namedParameters, innerObj);
	}

	private MapSqlParameterSource getSaveNamedParam(Integration integration) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		namedParameters.addValue("cid", integration.getCid());
		namedParameters.addValue("realm", integration.getRealm());
		namedParameters.addValue("lastModified", integration.getLastModified());
		namedParameters.addValue("active", integration.getActive());
		namedParameters.addValue("defaultUser", integration.getDefaultUser());

		return namedParameters;
	}

	private MapSqlParameterSource getUpdateNamedParam(Integration integration) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("integrationId", integration.getIntegrationId());
		namedParameters.addValue("cid", integration.getCid());
		namedParameters.addValue("realm", integration.getRealm());
		namedParameters.addValue("lastModified", integration.getLastModified());
		namedParameters.addValue("active", integration.getActive());
		namedParameters.addValue("defaultUser", integration.getDefaultUser());

		return namedParameters;
	}

}

package com.rol.dbservice.dao.impl;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.rol.dbservice.dao.MetaData;
import com.rol.dbservice.dao.rowmappers.RmDbServices;

@Component("rolmetadata")
public class MetaDataImpl  {

	@Autowired
	private NamedParameterJdbcTemplate template;
	

	public MetaData findMetaData() {
		final String query = "select * from gscalmetadata where metakey" ; 
		RmDbServices.MetaDataRm metadata= new RmDbServices().new MetaDataRm();
		List<MetaData> metadatadetails=template.query(query, metadata);
		MetaData meta=null;
		Optional<MetaData> findFirst = metadatadetails.stream().findFirst();
		if(findFirst.isPresent())
		{
			meta=findFirst.get();
		}
		return meta;
		
	}

	
	

	
}

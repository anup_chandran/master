package com.rol.sync.vo;

import com.rol.sync.vo.gmeeting.NotificationVo;

/**
 * @author Anup
 *
 */
public class GCalInput {

	public GCalInput(NotificationVo paramVo) {
		this.id = paramVo.getXGoogChannelID();
		this.resourceId = paramVo.getXGoogResourceID();
		this.resourceUri = paramVo.getXGoogResourceURI();
	}

	private String id;
	private String resourceId;
	private String resourceUri;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceUri() {
		return resourceUri;
	}

	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}

}

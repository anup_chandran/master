package com.rol.sync.vo.gmeeting;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "iconUri", "key", "name" })
public class ConferenceSolution {

	@JsonProperty("iconUri")
	private String iconUri;
	@JsonProperty("key")
	private Key key;
	@JsonProperty("name")
	private String name;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("iconUri")
	public String getIconUri() {
		return iconUri;
	}

	@JsonProperty("iconUri")
	public void setIconUri(String iconUri) {
		this.iconUri = iconUri;
	}

	@JsonProperty("key")
	public Key getKey() {
		return key;
	}

	@JsonProperty("key")
	public void setKey(Key key) {
		this.key = key;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
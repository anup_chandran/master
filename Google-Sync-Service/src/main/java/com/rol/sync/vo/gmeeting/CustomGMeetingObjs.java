package com.rol.sync.vo.gmeeting;

import com.rol.sync.vo.Booking;
import com.rol.sync.vo.SharedResource;
import com.rol.sync.vo.rol.ROLBookingRequest;

public class CustomGMeetingObjs {

	private SharedResource meetingRoom;

	private ROLBookingRequest meetingDetailsRol;

	private Booking dbBookingObj;

	public ROLBookingRequest getMeetingDetailsRol() {
		return meetingDetailsRol;
	}

	public void setMeetingDetailsRol(ROLBookingRequest meetingDetailsRol) {
		this.meetingDetailsRol = meetingDetailsRol;
	}

	public Booking getDbBookingObj() {
		return dbBookingObj;
	}

	public void setDbBookingObj(Booking dbBookingObj) {
		this.dbBookingObj = dbBookingObj;
	}

	public SharedResource getMeetingRoom() {
		return meetingRoom;
	}

	public void setMeetingRoom(SharedResource meetingRoom) {
		this.meetingRoom = meetingRoom;
	}

}

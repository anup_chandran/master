package com.rol.sync.vo;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.rol.ROLBookingResponse;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Booking {
	
	private int gsCalId;

	private Long rolBookingId;

	private Long rolLastModified;

	private String gscalUserICalUId;

	private String gscalUserEventId;

	private String gscalResourceEventId;

	private Long gscalLastModified;

	private Long sharedResourceId;

	private String gscalUserEmail;

	private String gscalUserName;

	private String rolUserEmail;

	private String isRecurrence;
	
	public Booking() {

	}
	
	public Booking(GSuiteMeeting gsm,ROLBookingResponse rolBkResp) {
		setGscalResourceEventId(gsm.getId());
		setIsRecurrence(Boolean.FALSE.toString());
		setSharedResourceId(gsm.getMeetingRoom().getRolResourceId());
		
		setRolBookingId(rolBkResp.getBid());
	}


	public int getGsCalId() {
		return gsCalId;
	}


	public void setGsCalId(int gsCalInfoId) {
		this.gsCalId = gsCalInfoId;
	}


	public Long getRolBookingId() {
		return rolBookingId;
	}


	public void setRolBookingId(Long rolBookingId) {
		this.rolBookingId = rolBookingId;
	}


	public Long getRolLastModified() {
		return rolLastModified;
	}


	public void setRolLastModified(Long rolLastModified) {
		this.rolLastModified = rolLastModified;
	}


	public String getGscalUserICalUId() {
		return gscalUserICalUId;
	}


	public void setGscalUserICalUId(String gscalUserICalUId) {
		this.gscalUserICalUId = gscalUserICalUId;
	}


	public String getGscalUserEventId() {
		return gscalUserEventId;
	}


	public void setGscalUserEventId(String gscalUserEventId) {
		this.gscalUserEventId = gscalUserEventId;
	}


	public String getGscalResourceEventId() {
		return gscalResourceEventId;
	}


	public void setGscalResourceEventId(String gscalResourceEventId) {
		this.gscalResourceEventId = gscalResourceEventId;
	}


	public Long getGscalLastModified() {
		return gscalLastModified;
	}


	public void setGscalLastModified(Long gscalLastModified) {
		this.gscalLastModified = gscalLastModified;
	}


	public Long getSharedResourceId() {
		return sharedResourceId;
	}


	public void setSharedResourceId(Long sharedResourceId) {
		this.sharedResourceId = sharedResourceId;
	}


	public String getGscalUserEmail() {
		return gscalUserEmail;
	}


	public void setGscalUserEmail(String gscalUserEmail) {
		this.gscalUserEmail = gscalUserEmail;
	}


	public String getGscalUserName() {
		return gscalUserName;
	}


	public void setGscalUserName(String gscalUserName) {
		this.gscalUserName = gscalUserName;
	}


	public String getRolUserEmail() {
		return rolUserEmail;
	}


	public void setRolUserEmail(String rolUserEmail) {
		this.rolUserEmail = rolUserEmail;
	}


	public String getIsRecurrence() {
		return isRecurrence;
	}


	public void setIsRecurrence(String isRecurrence) {
		this.isRecurrence = isRecurrence;
	}

}

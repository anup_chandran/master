package com.rol.sync.vo.gmeeting;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "X-Goog-Resource-ID", "X-Goog-Resource-State", "X-Goog-Channel-ID", "X-Goog-Channel-Expiration",
		"X-Goog-Message-Number", "X-Goog-Resource-URI" })
public class NotificationVo {

	@JsonProperty("X-Goog-Resource-ID")
	private String xGoogResourceID;
	@JsonProperty("X-Goog-Resource-State")
	private String xGoogResourceState;
	@JsonProperty("X-Goog-Channel-ID")
	private String xGoogChannelID;
	@JsonProperty("X-Goog-Channel-Expiration")
	private String xGoogChannelExpiration;
	@JsonProperty("X-Goog-Message-Number")
	private String xGoogMessageNumber;
	@JsonProperty("X-Goog-Resource-URI")
	private String xGoogResourceURI;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("X-Goog-Resource-ID")
	public String getXGoogResourceID() {
		return xGoogResourceID;
	}

	@JsonProperty("X-Goog-Resource-ID")
	public void setXGoogResourceID(String xGoogResourceID) {
		this.xGoogResourceID = xGoogResourceID;
	}

	@JsonProperty("X-Goog-Resource-State")
	public String getXGoogResourceState() {
		return xGoogResourceState;
	}

	@JsonProperty("X-Goog-Resource-State")
	public void setXGoogResourceState(String xGoogResourceState) {
		this.xGoogResourceState = xGoogResourceState;
	}

	@JsonProperty("X-Goog-Channel-ID")
	public String getXGoogChannelID() {
		return xGoogChannelID;
	}

	@JsonProperty("X-Goog-Channel-ID")
	public void setXGoogChannelID(String xGoogChannelID) {
		this.xGoogChannelID = xGoogChannelID;
	}

	@JsonProperty("X-Goog-Channel-Expiration")
	public String getXGoogChannelExpiration() {
		return xGoogChannelExpiration;
	}

	@JsonProperty("X-Goog-Channel-Expiration")
	public void setXGoogChannelExpiration(String xGoogChannelExpiration) {
		this.xGoogChannelExpiration = xGoogChannelExpiration;
	}

	@JsonProperty("X-Goog-Message-Number")
	public String getXGoogMessageNumber() {
		return xGoogMessageNumber;
	}

	@JsonProperty("X-Goog-Message-Number")
	public void setXGoogMessageNumber(String xGoogMessageNumber) {
		this.xGoogMessageNumber = xGoogMessageNumber;
	}

	@JsonProperty("X-Goog-Resource-URI")
	public String getXGoogResourceURI() {
		return xGoogResourceURI;
	}

	@JsonProperty("X-Goog-Resource-URI")
	public void setXGoogResourceURI(String xGoogResourceURI) {
		this.xGoogResourceURI = xGoogResourceURI;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}

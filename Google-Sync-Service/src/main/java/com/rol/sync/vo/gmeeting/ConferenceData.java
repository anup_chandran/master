package com.rol.sync.vo.gmeeting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "conferenceId", "conferenceSolution", "entryPoints", "signature" })
public class ConferenceData {

	@JsonProperty("conferenceId")
	private String conferenceId;
	@JsonProperty("conferenceSolution")
	private ConferenceSolution conferenceSolution;
	@JsonProperty("entryPoints")
	private List<EntryPoint> entryPoints = null;
	@JsonProperty("signature")
	private String signature;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("conferenceId")
	public String getConferenceId() {
		return conferenceId;
	}

	@JsonProperty("conferenceId")
	public void setConferenceId(String conferenceId) {
		this.conferenceId = conferenceId;
	}

	@JsonProperty("conferenceSolution")
	public ConferenceSolution getConferenceSolution() {
		return conferenceSolution;
	}

	@JsonProperty("conferenceSolution")
	public void setConferenceSolution(ConferenceSolution conferenceSolution) {
		this.conferenceSolution = conferenceSolution;
	}

	@JsonProperty("entryPoints")
	public List<EntryPoint> getEntryPoints() {
		return entryPoints;
	}

	@JsonProperty("entryPoints")
	public void setEntryPoints(List<EntryPoint> entryPoints) {
		this.entryPoints = entryPoints;
	}

	@JsonProperty("signature")
	public String getSignature() {
		return signature;
	}

	@JsonProperty("signature")
	public void setSignature(String signature) {
		this.signature = signature;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
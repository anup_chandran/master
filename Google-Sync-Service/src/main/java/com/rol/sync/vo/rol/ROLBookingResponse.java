package com.rol.sync.vo.rol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Bid", "Owner", "LastModified", "Created", "From", "Until", "Zid", "Status", "Parent",
		"ExternalId", "ZoneType", "Subject", "Desc", "Source", "Properties", "Attendees", "Private", "extensions",
		"NoShow" })
public class ROLBookingResponse {

	@JsonProperty("Bid")
	private Long bid;
	@JsonProperty("Owner")
	private String owner;
	@JsonProperty("LastModified")
	private Long lastModified;
	@JsonProperty("Created")
	private Long created;
	@JsonProperty("From")
	private Long from;
	@JsonProperty("Until")
	private Long until;
	@JsonProperty("Zid")
	private Long zid;
	@JsonProperty("Status")
	private String status;
	@JsonProperty("Parent")
	private Long parent;
	@JsonProperty("ExternalId")
	private String externalId;
	@JsonProperty("ZoneType")
	private String zoneType;
	@JsonProperty("Subject")
	private String subject;
	@JsonProperty("Desc")
	private String desc;
	@JsonProperty("Source")
	private String source;
	@JsonProperty("Properties")
	private Properties properties;
	@JsonProperty("Attendees")
	private List<Attendee> attendees = null;
	@JsonProperty("Private")
	private Boolean privateMeeting;
	@JsonProperty("NoShow")
	private String noShow;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonIgnore
	private int clientCode;

	@JsonProperty("Bid")
	public Long getBid() {
		return bid;
	}

	@JsonProperty("Bid")
	public void setBid(Long bid) {
		this.bid = bid;
	}

	@JsonProperty("Owner")
	public String getOwner() {
		return owner;
	}

	@JsonProperty("Owner")
	public void setOwner(String owner) {
		this.owner = owner;
	}

	@JsonProperty("LastModified")
	public Long getLastModified() {
		return lastModified;
	}

	@JsonProperty("LastModified")
	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	@JsonProperty("Created")
	public Long getCreated() {
		return created;
	}

	@JsonProperty("Created")
	public void setCreated(Long created) {
		this.created = created;
	}

	@JsonProperty("From")
	public Long getFrom() {
		return from;
	}

	@JsonProperty("From")
	public void setFrom(Long from) {
		this.from = from;
	}

	@JsonProperty("Until")
	public Long getUntil() {
		return until;
	}

	@JsonProperty("Until")
	public void setUntil(Long until) {
		this.until = until;
	}

	@JsonProperty("Zid")
	public Long getZid() {
		return zid;
	}

	@JsonProperty("Zid")
	public void setZid(Long zid) {
		this.zid = zid;
	}

	@JsonProperty("Status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("Status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("Parent")
	public Long getParent() {
		return parent;
	}

	@JsonProperty("Parent")
	public void setParent(Long parent) {
		this.parent = parent;
	}

	@JsonProperty("ExternalId")
	public String getExternalId() {
		return externalId;
	}

	@JsonProperty("ExternalId")
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@JsonProperty("ZoneType")
	public String getZoneType() {
		return zoneType;
	}

	@JsonProperty("ZoneType")
	public void setZoneType(String zoneType) {
		this.zoneType = zoneType;
	}

	@JsonProperty("Subject")
	public String getSubject() {
		return subject;
	}

	@JsonProperty("Subject")
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@JsonProperty("Desc")
	public String getDesc() {
		return desc;
	}

	@JsonProperty("Desc")
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@JsonProperty("Source")
	public String getSource() {
		return source;
	}

	@JsonProperty("Source")
	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("Properties")
	public Properties getProperties() {
		return properties;
	}

	@JsonProperty("Properties")
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	@JsonProperty("Attendees")
	public List<Attendee> getAttendees() {
		return attendees;
	}

	@JsonProperty("Attendees")
	public void setAttendees(List<Attendee> attendees) {
		this.attendees = attendees;
	}

	@JsonProperty("Private")
	public Boolean getPrivate() {
		return privateMeeting;
	}

	@JsonProperty("Private")
	public void setPrivate(Boolean privateMeeting) {
		this.privateMeeting = privateMeeting;
	}

	@JsonProperty("NoShow")
	public String getNoShow() {
		return noShow;
	}

	@JsonProperty("NoShow")
	public void setNoShow(String noShow) {
		this.noShow = noShow;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public int getClientCode() {
		return clientCode;
	}

	public void setClientCode(int clientCode) {
		this.clientCode = clientCode;
	}

}
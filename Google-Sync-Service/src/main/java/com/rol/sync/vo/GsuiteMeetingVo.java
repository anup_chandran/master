package com.rol.sync.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.rol.sync.vo.gmeeting.Attendee;
import com.rol.sync.vo.gmeeting.ConferenceData;
import com.rol.sync.vo.gmeeting.Creator;
import com.rol.sync.vo.gmeeting.End;
import com.rol.sync.vo.gmeeting.Organizer;
import com.rol.sync.vo.gmeeting.Reminders;
import com.rol.sync.vo.gmeeting.Start;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "attendees", "conferenceData", "created", "creator", "end", "etag", "hangoutLink", "htmlLink",
		"iCalUID", "id", "kind", "location", "organizer", "reminders", "sequence", "start", "status", "summary",
		"updated" })
public class GsuiteMeetingVo {

	@JsonProperty("attendees")
	private List<Attendee> attendees = null;
	@JsonProperty("conferenceData")
	private ConferenceData conferenceData;
	@JsonProperty("created")
	private String created;
	@JsonProperty("creator")
	private Creator creator;
	@JsonProperty("end")
	private End end;
	@JsonProperty("etag")
	private String etag;
	@JsonProperty("hangoutLink")
	private String hangoutLink;
	@JsonProperty("htmlLink")
	private String htmlLink;
	@JsonProperty("iCalUID")
	private String iCalUID;
	@JsonProperty("id")
	private String id;
	@JsonProperty("kind")
	private String kind;
	@JsonProperty("location")
	private String location;
	@JsonProperty("organizer")
	private Organizer organizer;
	@JsonProperty("reminders")
	private Reminders reminders;
	@JsonProperty("sequence")
	private Long sequence;
	@JsonProperty("start")
	private Start start;
	@JsonProperty("status")
	private String status;
	@JsonProperty("summary")
	private String summary;
	@JsonProperty("updated")
	private String updated;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("attendees")
	public List<Attendee> getAttendees() {
		return attendees;
	}

	@JsonProperty("attendees")
	public void setAttendees(List<Attendee> attendees) {
		this.attendees = attendees;
	}

	@JsonProperty("conferenceData")
	public ConferenceData getConferenceData() {
		return conferenceData;
	}

	@JsonProperty("conferenceData")
	public void setConferenceData(ConferenceData conferenceData) {
		this.conferenceData = conferenceData;
	}

	@JsonProperty("created")
	public String getCreated() {
		return created;
	}

	@JsonProperty("created")
	public void setCreated(String created) {
		this.created = created;
	}

	@JsonProperty("creator")
	public Creator getCreator() {
		return creator;
	}

	@JsonProperty("creator")
	public void setCreator(Creator creator) {
		this.creator = creator;
	}

	@JsonProperty("end")
	public End getEnd() {
		return end;
	}

	@JsonProperty("end")
	public void setEnd(End end) {
		this.end = end;
	}

	@JsonProperty("etag")
	public String getEtag() {
		return etag;
	}

	@JsonProperty("etag")
	public void setEtag(String etag) {
		this.etag = etag;
	}

	@JsonProperty("hangoutLink")
	public String getHangoutLink() {
		return hangoutLink;
	}

	@JsonProperty("hangoutLink")
	public void setHangoutLink(String hangoutLink) {
		this.hangoutLink = hangoutLink;
	}

	@JsonProperty("htmlLink")
	public String getHtmlLink() {
		return htmlLink;
	}

	@JsonProperty("htmlLink")
	public void setHtmlLink(String htmlLink) {
		this.htmlLink = htmlLink;
	}

	@JsonProperty("iCalUID")
	public String getICalUID() {
		return iCalUID;
	}

	@JsonProperty("iCalUID")
	public void setICalUID(String iCalUID) {
		this.iCalUID = iCalUID;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("kind")
	public String getKind() {
		return kind;
	}

	@JsonProperty("kind")
	public void setKind(String kind) {
		this.kind = kind;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	@JsonProperty("location")
	public void setLocation(String location) {
		this.location = location;
	}

	@JsonProperty("organizer")
	public Organizer getOrganizer() {
		return organizer;
	}

	@JsonProperty("organizer")
	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	@JsonProperty("reminders")
	public Reminders getReminders() {
		return reminders;
	}

	@JsonProperty("reminders")
	public void setReminders(Reminders reminders) {
		this.reminders = reminders;
	}

	@JsonProperty("sequence")
	public Long getSequence() {
		return sequence;
	}

	@JsonProperty("sequence")
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	@JsonProperty("start")
	public Start getStart() {
		return start;
	}

	@JsonProperty("start")
	public void setStart(Start start) {
		this.start = start;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("summary")
	public String getSummary() {
		return summary;
	}

	@JsonProperty("summary")
	public void setSummary(String summary) {
		this.summary = summary;
	}

	@JsonProperty("updated")
	public String getUpdated() {
		return updated;
	}

	@JsonProperty("updated")
	public void setUpdated(String updated) {
		this.updated = updated;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
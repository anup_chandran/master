package com.rol.sync.vo;

public class SharedResource {

	public SharedResource() {
	}

	
	private String gcalemail;
	private String timezone;
	private Long companyid;

	private String gchannelId;
	private String gresourceId;
	private String gresourceUri;
	private String gexpiration;

	private String gsynctoken;

	private Long rolResourceId;

	public String getTimeZone() {
		return timezone;
	}

	public void setTimeZone(String timeZone) {
		this.timezone = timeZone;
	}

	public String getGcalResourceEmail() {
		return gcalemail;
	}

	public void setGcalResourceEmail(String gcalResourceEmail) {
		this.gcalemail = gcalResourceEmail;
	}

	public void setGcalemail(String gcalemail) {
		this.gcalemail = gcalemail;
	}

	public String getGchannelId() {
		return gchannelId;
	}

	public void setGchannelId(String gchannelId) {
		this.gchannelId = gchannelId;
	}

	public String getGresourceId() {
		return gresourceId;
	}

	public void setGresourceId(String gresourceId) {
		this.gresourceId = gresourceId;
	}

	public String getGresourceUri() {
		return gresourceUri;
	}

	public void setGresourceUri(String gresourceUti) {
		this.gresourceUri = gresourceUti;
	}

	public String getGexpiration() {
		return gexpiration;
	}

	public void setGexpiration(String gexpiration) {
		this.gexpiration = gexpiration;
	}

	public String getGsynctoken() {
		return gsynctoken;
	}

	public void setGsynctoken(String gsynctoken) {
		this.gsynctoken = gsynctoken;
	}

	public Long getRolResourceId() {
		return rolResourceId;
	}

	public void setRolResourceId(Long rolResourceId) {
		this.rolResourceId = rolResourceId;
	}

	public Long getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Long companyid) {
		this.companyid = companyid;
	}

}

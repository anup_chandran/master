package com.rol.sync.vo;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.gmeeting.NotificationVo;

/**
 * @author Anup
 *
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SyncTaskVo {

	private String notification;
	private NotificationVo notificationVo;

	private List<GSuiteMeeting> meetings;

	

	private boolean recurrence;

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public NotificationVo getNotificationVo() {
		return notificationVo;
	}

	public void setNotificationVo(NotificationVo notificationVo) {
		this.notificationVo = notificationVo;
	}

	public boolean isRecurrence() {
		return recurrence;
	}

	public void setRecurrence(boolean recurrence) {
		this.recurrence = recurrence;
	}

	public List<GSuiteMeeting> getMeetings() {
		return meetings;
	}

	public void setMeetings(List<GSuiteMeeting> meetings) {
		this.meetings = meetings;
	}

}

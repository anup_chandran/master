package com.rol.sync.vo.gmeeting;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.api.client.util.DateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "date", "dateTime", "timeZone" })
public class EventDateTime {

	@JsonProperty("date")
	private DateTime date;
	@JsonProperty("dateTime")
	private DateTime dateTime;
	@JsonProperty("timeZone")
	private java.lang.String timeZone;

	@JsonProperty("date")
	public DateTime getDate() {
		return date;
	}

	@JsonProperty("date")
	public void setDate(DateTime date) {
		this.date = date;
	}

	@JsonProperty("dateTime")
	public DateTime getDateTime() {
		return dateTime;
	}

	@JsonProperty("dateTime")
	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	@JsonProperty("timeZone")
	public java.lang.String getTimeZone() {
		return timeZone;
	}

	@JsonProperty("timeZone")
	public void setTimeZone(java.lang.String timeZone) {
		this.timeZone = timeZone;
	}

}

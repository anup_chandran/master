package com.rol.sync.vo.gmeeting;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "displayName", "email", "resource", "responseStatus" })
public class Attendee {

	@JsonProperty("displayName")
	private String displayName;
	@JsonProperty("email")
	private String email;
	@JsonProperty("resource")
	private Boolean resource;
	@JsonProperty("responseStatus")
	private String responseStatus;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("displayName")
	public String getDisplayName() {
		return displayName;
	}

	@JsonProperty("displayName")
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("resource")
	public Boolean getResource() {
		return resource;
	}

	@JsonProperty("resource")
	public void setResource(Boolean resource) {
		this.resource = resource;
	}

	@JsonProperty("responseStatus")
	public String getResponseStatus() {
		return responseStatus;
	}

	@JsonProperty("responseStatus")
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}


}
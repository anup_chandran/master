package com.rol.sync.vo.gmeeting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "attendees", "conferenceData", "created", "creator", "description", "end", "etag", "hangoutLink",
		"htmlLink", "iCalUID", "id", "kind", "location", "organizer", "reminders", "sequence", "start", "status",
		"summary", "updated", "visibility" })
public class GSuiteMeeting extends CustomGMeetingObjs{

	@JsonProperty("attendees")
	private List<Attendee> attendees = null;
	@JsonProperty("conferenceData")
	private ConferenceData conferenceData;
	@JsonProperty("created")
	private String created;
	@JsonProperty("creator")
	private Creator creator;
	@JsonProperty("description")
	private String description;
	@JsonProperty("end")
	private End end;
	@JsonProperty("etag")
	private String etag;
	@JsonProperty("hangoutLink")
	private String hangoutLink;
	@JsonProperty("htmlLink")
	private String htmlLink;
	@JsonProperty("iCalUID")
	private String iCalUID;
	@JsonProperty("id")
	private String id;
	@JsonProperty("kind")
	private String kind;
	@JsonProperty("location")
	private String location;
	@JsonProperty("organizer")
	private Organizer organizer;
	@JsonProperty("reminders")
	private Reminders reminders;
	@JsonProperty("sequence")
	private Integer sequence;
	@JsonProperty("start")
	private Start start;
	@JsonProperty("status")
	private String status;
	@JsonProperty("summary")
	private String summary;
	@JsonProperty("updated")
	private String updated;
	@JsonProperty("visibility")
	private String visibility;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("attendees")
	public List<Attendee> getAttendees() {
		return attendees;
	}

	@JsonProperty("attendees")
	public void setAttendees(List<Attendee> attendees) {
		this.attendees = attendees;
	}

	@JsonProperty("conferenceData")
	public ConferenceData getConferenceData() {
		return conferenceData;
	}

	@JsonProperty("conferenceData")
	public void setConferenceData(ConferenceData conferenceData) {
		this.conferenceData = conferenceData;
	}

	@JsonProperty("created")
	public String getCreated() {
		return created;
	}

	@JsonProperty("created")
	public void setCreated(String created) {
		this.created = created;
	}

	@JsonProperty("creator")
	public Creator getCreator() {
		return creator;
	}

	@JsonProperty("creator")
	public void setCreator(Creator creator) {
		this.creator = creator;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("end")
	public End getEnd() {
		return end;
	}

	@JsonProperty("end")
	public void setEnd(End end) {
		this.end = end;
	}

	@JsonProperty("etag")
	public String getEtag() {
		return etag;
	}

	@JsonProperty("etag")
	public void setEtag(String etag) {
		this.etag = etag;
	}

	@JsonProperty("hangoutLink")
	public String getHangoutLink() {
		return hangoutLink;
	}

	@JsonProperty("hangoutLink")
	public void setHangoutLink(String hangoutLink) {
		this.hangoutLink = hangoutLink;
	}

	@JsonProperty("htmlLink")
	public String getHtmlLink() {
		return htmlLink;
	}

	@JsonProperty("htmlLink")
	public void setHtmlLink(String htmlLink) {
		this.htmlLink = htmlLink;
	}

	@JsonProperty("iCalUID")
	public String getICalUID() {
		return iCalUID;
	}

	@JsonProperty("iCalUID")
	public void setICalUID(String iCalUID) {
		this.iCalUID = iCalUID;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("kind")
	public String getKind() {
		return kind;
	}

	@JsonProperty("kind")
	public void setKind(String kind) {
		this.kind = kind;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	@JsonProperty("location")
	public void setLocation(String location) {
		this.location = location;
	}

	@JsonProperty("organizer")
	public Organizer getOrganizer() {
		return organizer;
	}

	@JsonProperty("organizer")
	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	@JsonProperty("reminders")
	public Reminders getReminders() {
		return reminders;
	}

	@JsonProperty("reminders")
	public void setReminders(Reminders reminders) {
		this.reminders = reminders;
	}

	@JsonProperty("sequence")
	public Integer getSequence() {
		return sequence;
	}

	@JsonProperty("sequence")
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	@JsonProperty("start")
	public Start getStart() {
		return start;
	}

	@JsonProperty("start")
	public void setStart(Start start) {
		this.start = start;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("summary")
	public String getSummary() {
		return summary;
	}

	@JsonProperty("summary")
	public void setSummary(String summary) {
		this.summary = summary;
	}

	@JsonProperty("updated")
	public String getUpdated() {
		return updated;
	}

	@JsonProperty("updated")
	public void setUpdated(String updated) {
		this.updated = updated;
	}

	@JsonProperty("visibility")
	public String getVisibility() {
		return visibility;
	}

	@JsonProperty("visibility")
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "GSuiteMeeting [attendees=" + attendees + ", conferenceData=" + conferenceData + ", created=" + created
				+ ", creator=" + creator + ", description=" + description + ", end=" + end + ", etag=" + etag
				+ ", hangoutLink=" + hangoutLink + ", htmlLink=" + htmlLink + ", iCalUID=" + iCalUID + ", id=" + id
				+ ", kind=" + kind + ", location=" + location + ", organizer=" + organizer + ", reminders=" + reminders
				+ ", sequence=" + sequence + ", start=" + start + ", status=" + status + ", summary=" + summary
				+ ", updated=" + updated + ", visibility=" + visibility + ", additionalProperties="
				+ additionalProperties + "]";
	}

}
package com.rol.sync.conf;

import java.util.concurrent.Executor;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@Component
public class GsyncTaskPool {

	private ThreadPoolTaskExecutor threadPoolexecutor;

	private Logger logger = LoggerFactory.getLogger(GsyncTaskPool.class);

	@PostConstruct
	private void init() {
		threadPoolexecutor = new ThreadPoolTaskExecutor();

		threadPoolexecutor.setCorePoolSize(10);
		threadPoolexecutor.setMaxPoolSize(100);
		threadPoolexecutor.setThreadNamePrefix("ROLServerLookup-");
//		threadPoolexecutor.setQueueCapacity(1);

		threadPoolexecutor.initialize();
	}

	/**
	 * @return
	 */
	public Executor getExecutor() {

		return threadPoolexecutor;
	}

	/**
	 * Return true if all thread are occupied , Make sure the server does not accept
	 * the notification for processing if cannot handle
	 * 
	 * @return
	 */
	public boolean areAllThreadActive() {

		
		if (logger.isDebugEnabled()) {
			logger.debug("Queue Remaining Capacity {}",
					threadPoolexecutor.getThreadPoolExecutor().getQueue().remainingCapacity());
			logger.debug("Active Count {}", threadPoolexecutor.getActiveCount());
		}
		 
		return ((threadPoolexecutor.getThreadPoolExecutor().getQueue().remainingCapacity() == 0)
				&& (threadPoolexecutor.getActiveCount() == threadPoolexecutor.getMaxPoolSize())) ; 


	}

}

package com.rol.sync.conf;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.rol.sync.executors.GsyncTaskExecutor;
import com.rol.sync.vo.SyncTaskVo;
import com.rol.sync.vo.gmeeting.NotificationVo;

/**
 * @author Anup
 * 
 *         Accepts the JSON processing
 *
 */
@Component
@EnableScheduling
public class SyncTaskQueue {

	@Autowired
	@Qualifier("GSynTaskExecutor")
	private Executor taskExecutor;

	@Autowired
	private ObjectFactory<GsyncTaskExecutor> gSyncExecFac;
	@Autowired
	private ObjectFactory<SyncTaskVo> syncTaskVoFac;

	@Autowired
	private Gson gson;
	// Key will be resource/Channel Id
	private Map<String, SyncTaskVo> watchBucket = Collections.synchronizedMap(new HashMap<>());
	@Autowired
	private ObjectMapper objectMapper;
	
	ReentrantLock lock = new ReentrantLock() ;

	/**
	 * Add the notification JSON to the queue for processing
	 * 
	 * @param payload
	 */
	public void add(final String payload) {
		SyncTaskVo syncTaskVo = syncTaskVoFac.getObject();

		try {
			syncTaskVo.setNotification(payload);
			syncTaskVo.setNotificationVo(objectMapper.readValue(payload, NotificationVo.class));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		syncTaskVo.setNotificationVo(gson.fromJson(payload, NotificationVo.class)) ;
		watchBucket.put(syncTaskVo.getNotificationVo().getXGoogResourceID(), syncTaskVo);

//		executeTask(syncTaskVo);
	}

	/**
	 * Execute if after 10 Seconds ,delayed execution
	 * 
	 * @param syncTaskVo
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 */
	@Scheduled(fixedDelay = 10000)
	void executeTask()  {
		
			if (watchBucket.isEmpty()) {// No notifications registered return
				return;
			}
			
			if(lock.tryLock()) {
				try {
					Collection<SyncTaskVo> colSyncTaskVo = watchBucket.values();
					Iterator<SyncTaskVo> itrSyncVo = colSyncTaskVo.iterator();
					
					while (itrSyncVo.hasNext()) {
						SyncTaskVo syncTaskVo = itrSyncVo.next();
						SyncTaskVo syncTaskVoPass = syncTaskVoFac.getObject();
						// Clear the Watch bucker for the executing resource
						itrSyncVo.remove();
						
						syncTaskVoPass.setNotification(syncTaskVo.getNotification());
						// Execute the task
						taskExecutor.execute(getRunnableObj(syncTaskVoPass));
					}
				}finally {
					lock.unlock();
				}
			}
		

	}

	/**
	 * Returns the Task executor to execute the notification request
	 * 
	 * @param payload
	 * @return
	 */
	private GsyncTaskExecutor getRunnableObj(final SyncTaskVo syncTaskVo) {
		GsyncTaskExecutor gtaskExec = gSyncExecFac.getObject();
		gtaskExec.setSyncVo(syncTaskVo);
		return gtaskExec;
	}

}

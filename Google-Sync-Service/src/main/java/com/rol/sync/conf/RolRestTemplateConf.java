package com.rol.sync.conf;

import java.util.Collections;

import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.rol.sync.interceptors.RestClientInterceptor;	

@Component("RolRestTemplateConf")
public class RolRestTemplateConf implements RestTemplateCustomizer {

	@Override
	public void customize(RestTemplate restTemplate) {
		
		HttpComponentsClientHttpRequestFactory clientReqFactory = new HttpComponentsClientHttpRequestFactory() ;
		
		clientReqFactory.setReadTimeout(5000);
		clientReqFactory.setConnectTimeout(5000);
		
		 ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(clientReqFactory); 
		 restTemplate.setRequestFactory(factory);
		 
		restTemplate.setInterceptors(Collections.singletonList(new RestClientInterceptor()));
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

	}

}

package com.rol.sync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class GoogleSyncServiceApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(GoogleSyncServiceApplication.class, args);
	}

}

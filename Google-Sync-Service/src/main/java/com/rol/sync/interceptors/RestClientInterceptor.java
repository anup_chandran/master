package com.rol.sync.interceptors;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import com.rol.sync.logcontext.MdcUtil;

public class RestClientInterceptor implements ClientHttpRequestInterceptor {
	private static Logger logger = LoggerFactory.getLogger(RestClientInterceptor.class);

	

	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		logRequestDetails(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		logResponseDetails(response);
		return response;
	}

	/**
	 * @param request
	 * @param body
	 */
	private void logRequestDetails(HttpRequest request, byte[] body) {
		if (logger.isInfoEnabled()) {
			MdcUtil.addSingleEntry("URI         : ", request.getURI());
			MdcUtil.addSingleEntry("Method      : ", request.getMethod());
			MdcUtil.addSingleEntry("Headers     : ", request.getHeaders());
			MdcUtil.addSingleEntry("Request body: ", new String(body, StandardCharsets.UTF_8));
		}
		
	}

	/**
	 * @param response
	 */
	private void logResponseDetails(ClientHttpResponse response) {
		try {
			if (logger.isInfoEnabled()) {
				MdcUtil.addSingleEntry("Status code  : ", response.getStatusCode());
				MdcUtil.addSingleEntry("Status text  : ", response.getStatusText());
				MdcUtil.addSingleEntry("Headers      : ", response.getHeaders());
				MdcUtil.addSingleEntry("Response body: ",
						StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
			}
		} catch (IOException e) {
			logger.error("Could not log response");
		} finally {
			logger.info("Request/Respose Summary");
			MdcUtil.clearMdc();
		}
		
	}

}

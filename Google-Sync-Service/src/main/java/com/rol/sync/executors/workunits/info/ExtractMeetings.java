package com.rol.sync.executors.workunits.info;

import java.util.List;
import java.util.Objects;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.executors.workunits.SyncWorkUnit;
import com.rol.sync.vo.Booking;
import com.rol.sync.vo.SharedResource;
import com.rol.sync.vo.gmeeting.Attendee;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.gmeeting.NotificationVo;

/**
 * Retrieve the meeting details from the Google Calendar Service , using the
 * syncToken
 * 
 * @author Anup
 *
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExtractMeetings extends SyncWorkUnit {

	@Autowired
	private RestTemplate restTemplate;
	@Value("${gCal_booking_service}")
	private String gCalUrl;
	@Autowired
	private ObjectMapper objectMapper;

	Logger logger = LoggerFactory.getLogger(ExtractMeetings.class);

	@Override
	public void handle() throws GsyncException {
		getDetailsGSuite();

		super.callSuccesor();
	}

	/**
	 * @param syncTaskVo
	 * @throws GsyncException
	 */
	private void getDetailsGSuite() throws GsyncException {
		try {
			syncTaskVo.setNotificationVo(objectMapper.readValue(syncTaskVo.getNotification(), NotificationVo.class));
		} catch (JsonProcessingException e) {
			logger.error("Error Parsing the information !!", e);
		}
		// get Meeting details
		ResponseEntity<String> respEntity = restTemplate.getForEntity(
				"http://localhost:7000/gcal/room/updated/events/" + syncTaskVo.getNotificationVo().getXGoogResourceID(),
				String.class);
		try {
			getSyncTaskVo()
					.setMeetings(objectMapper.readValue(respEntity.getBody(), new TypeReference<List<GSuiteMeeting>>() {
					}));
		} catch (JsonProcessingException e) {
			logger.error("Error Parsing the meeting List using the sync token", e);
		}

		if (respEntity.getStatusCode().isError()) {
			GsyncException gSyncExcep = new GsyncException();
			gSyncExcep.getTokenErrorBean().setErrorCode(String.valueOf(respEntity.getStatusCodeValue()));
			throw gSyncExcep;
		}
		
		for (GSuiteMeeting gMeeting : getSyncTaskVo().getMeetings()) {
			// Set all the meeting rooms
			setAllMeetingRooms(gMeeting) ;
			setDbObject(gMeeting) ;
		}

	}
	
	private void setDbObject(GSuiteMeeting gMeeting) {
		// Set the db booking object , if available
		ResponseEntity<Booking> respEntityDb = restTemplate.getForEntity("http://localhost:8080/db/booking/bookingid/"+gMeeting.getId()+"/roomid/"+gMeeting.getMeetingRoom().getRolResourceId(),Booking.class);
		if(HttpStatus.SC_NO_CONTENT != respEntityDb.getStatusCode().value()) {
			gMeeting.setDbBookingObj(respEntityDb.getBody());
		}
	}

	/**
	 * @param gMeeting
	 */
	private void setAllMeetingRooms(GSuiteMeeting gMeeting) {
			
			ResponseEntity<SharedResource> respEntity = restTemplate
					.getForEntity("http://localhost:8080/db/room/gid/"
							+ syncTaskVo.getNotificationVo().getXGoogResourceID(), SharedResource.class);
			gMeeting.setMeetingRoom(respEntity.getBody());
			
			return ;
		/*
		 * for (Attendee attendee : gMeeting.getAttendees()) { if
		 * (Objects.nonNull(attendee.getResource()) &&
		 * attendee.getResource().booleanValue()) { // get Meeting details
		 * 
		 * } }
		 */
	}

	@Override
	protected boolean canHandle() throws GsyncException {
		return false;
	}

}

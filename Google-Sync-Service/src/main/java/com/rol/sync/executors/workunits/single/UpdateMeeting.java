package com.rol.sync.executors.workunits.single;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.executors.workunits.SyncMeeting;
import com.rol.sync.executors.workunits.SyncWorkUnit;
import com.rol.sync.util.RolReqJsonGenerator;
import com.rol.sync.vo.Booking;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.rol.ROLBookingRequest;

/**
 * @author Anup
 *
 */
@Component("UpdateMeeting")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UpdateMeeting extends SyncWorkUnit {

	private final Logger logger = LoggerFactory.getLogger(UpdateMeeting.class);
	@Autowired
	private RolReqJsonGenerator rolReqGenerator;

	@Value("${rol_booking_service}")
	private String bookingUrl;

	@Autowired
	@Qualifier("SyncMeetingImpl")
	private SyncMeeting syncOperations;

	private List<GSuiteMeeting> updateMeetingLst = new ArrayList<>();

	@Override
	@Async
	protected void handle() throws GsyncException {

		if (canHandle()) {
			logger.info("In the update Single Occurence !!");

			updateMeetingLst.parallelStream().forEach(t -> {
				try {
					update(t);
				} catch (GsyncException e) {
					logger.error("Error during parallel processing of Meeting list", t.toString(), e);
				}
			});

		}

		callSuccesor();
	}

	/**
	 * Checks if this scenario can be handled
	 */
	@Override
	protected boolean canHandle() throws GsyncException {
		// Loop the list and return true if there are meetings which needs to be created
		Iterator<GSuiteMeeting> itr = getSyncTaskVo().getMeetings().iterator();
		GSuiteMeeting gMeeting = null;
		while (itr.hasNext()) {
			gMeeting = itr.next();
			if ("confirmed".equals(gMeeting.getStatus()) && Objects.nonNull(gMeeting.getDbBookingObj())) {
				updateMeetingLst.add(gMeeting);
				// Remove the entry from the main list , as it has been processed
				itr.remove();
			}
		}

		return !updateMeetingLst.isEmpty();
	}

	/**
	 * Send the patch request to the ROL rest server
	 * 
	 * @throws GsyncException
	 */
	private void update(GSuiteMeeting gsuiteMeeting) throws GsyncException {
		
		// Handle the Sync operation.
		ROLBookingRequest rolBookingReq = rolReqGenerator.singleMeetingUpdateReq(gsuiteMeeting);
		syncOperations.updateMeetingReqRol(rolBookingReq).join();

	}

}

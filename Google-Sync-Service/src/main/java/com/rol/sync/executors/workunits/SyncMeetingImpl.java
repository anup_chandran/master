package com.rol.sync.executors.workunits;

import java.util.concurrent.CompletableFuture;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.vo.Booking;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.rol.ROLBookingRequest;
import com.rol.sync.vo.rol.ROLBookingResponse;

@Component("SyncMeetingImpl")
@Scope("prototype")
@EnableAsync
public class SyncMeetingImpl implements SyncMeeting {
	
	
	@Autowired
	private RestTemplate restTemplate ;
	
	@Value("${gCal_booking_service}")
	private String gCalUrl;
	
	@Value("${rol_booking_service}")
	private String bookingUrl ;
	
	private Logger logger = LoggerFactory.getLogger(SyncMeetingImpl.class) ;
	



	@Override
	@Async
	public CompletableFuture<ROLBookingResponse> createMeetingReqRol(GSuiteMeeting gsm) throws GsyncException {

		ResponseEntity<ROLBookingResponse> respEntity = restTemplate.postForEntity(bookingUrl,
				gsm.getMeetingDetailsRol(), ROLBookingResponse.class);

		if (respEntity.getStatusCode().isError()) {
			GsyncException gSyncExcep = new GsyncException();
			gSyncExcep.getTokenErrorBean().setErrorCode(String.valueOf(respEntity.getStatusCodeValue()));
			throw gSyncExcep;
		}
		
		return CompletableFuture.completedFuture(respEntity.getBody()) ;
	}
	

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	@Override
	@Async
	public CompletableFuture<ResponseEntity<String>> createDb(Booking booking) throws GsyncException {
		ResponseEntity<String> respEntity = restTemplate.postForEntity("http://localhost:8080/db/booking", booking, String.class);
		
		if (respEntity.getStatusCode().isError()) {
			GsyncException gSyncExcep = new GsyncException();
			gSyncExcep.getTokenErrorBean().setErrorCode(String.valueOf(respEntity.getStatusCodeValue()));
			throw gSyncExcep;
		}
		
		return CompletableFuture.completedFuture(respEntity) ;
	}
	
	@Override
	@Async
	public CompletableFuture<String> updateMeetingReqRol(ROLBookingRequest rolBookingReq) throws GsyncException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		HttpEntity<ROLBookingRequest> request = new HttpEntity<>(rolBookingReq, headers);
		
		logger.info("initializing the Request for {}",rolBookingReq.getEventId());
		
		ResponseEntity<String> respEntity = restTemplate.exchange(
				bookingUrl.concat(String.valueOf(rolBookingReq.getBid())), HttpMethod.PUT, request,
				String.class);
	
		if (respEntity.getStatusCode().isError()) {
			GsyncException gSyncExcep = new GsyncException();
			gSyncExcep.getTokenErrorBean().setErrorCode(String.valueOf(respEntity.getStatusCodeValue()));
			throw gSyncExcep;
		}
		
		return CompletableFuture.completedFuture(respEntity.getBody());
	}
	
	@Override
	@Async
	public CompletableFuture<Integer> delete(String rolBookingId) throws GsyncException {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		HttpEntity<ROLBookingRequest> request = new HttpEntity<>(headers);

		ResponseEntity<String> respEntity = restTemplate.exchange(
				bookingUrl.concat(rolBookingId), HttpMethod.DELETE,
				request, String.class);
		if (respEntity.getStatusCode().isError()) {
			GsyncException gSyncExcep = new GsyncException();
			gSyncExcep.getTokenErrorBean().setErrorCode(String.valueOf(respEntity.getStatusCodeValue()));
			throw gSyncExcep;
		}
		
		return CompletableFuture.completedFuture(respEntity.getStatusCodeValue()) ;
	}
	
	@Override
	@Async
	public CompletableFuture<Booking> bookingInfoDb(String meetingId, String roomId) {
		// Set the db booking object , if available
		ResponseEntity<Booking> respEntityDb = restTemplate.getForEntity("http://localhost:8080/db/booking/bookingid/"+meetingId+"/roomid/"+roomId,Booking.class);
		if(HttpStatus.SC_NO_CONTENT != respEntityDb.getStatusCode().value()) {
			return CompletableFuture.completedFuture(respEntityDb.getBody());
		}
		
		return CompletableFuture.completedFuture(new Booking());
	}

}

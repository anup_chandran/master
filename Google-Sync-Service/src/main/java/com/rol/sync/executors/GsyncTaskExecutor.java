package com.rol.sync.executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.executors.workunits.WorkUnitsFlowSync;
import com.rol.sync.vo.SyncTaskVo;

/**
 * Executes the required task to handle the watch notification from GSuite
 * 
 * @author Anup
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GsyncTaskExecutor implements Runnable {

	@Autowired
	private SyncTaskVo syncVo;
	
	@Autowired
	private WorkUnitsFlowSync workUnitsExec;
	
	private Logger logger = LoggerFactory.getLogger(GsyncTaskExecutor.class);

	/**
	 * Executes the required task to handle the watch notification from GSuite
	 */
	@Override
	public void run() {
		try {
			workUnitsExec.execute(getSyncVo());
		} catch (GsyncException e ) {
			logger.error("Reporting Error - Thread Executor -{} !",e.getTokenErrorBean().getErrorMessage(), e);
		}catch (Exception e) {
			logger.error("Reporting Un-Caught Exception Error at the Thread Executor !", e);
		}
	}

	public SyncTaskVo getSyncVo() {
		return syncVo;
	}

	public void setSyncVo(SyncTaskVo syncVo) {
		this.syncVo = syncVo;
	}

}

package com.rol.sync.executors.workunits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.vo.gmeeting.NotificationVo;

/**
 * Handles the validation required for the flow
 * @author Anup
 *
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Validate extends SyncWorkUnit {
	@Autowired
	private Gson gson ;

	@Override
	public void handle() throws GsyncException {
		if(canHandle()) {
			syncTaskVo.setNotificationVo(gson.fromJson(syncTaskVo.getNotification(), NotificationVo.class));
			verifyNotificationChannel() ;
		}
		
		callSuccesor();
	}
	
	/**
	 * Verify that the Notification channel exists in Database
	 */
	private void verifyNotificationChannel()  {
		//TODO The secret key matches , our records 
		//TODO Implement the DB Call and verify channel is registered
	}

	@Override
	protected boolean canHandle() throws GsyncException {
		return true;
	}

}

package com.rol.sync.executors.workunits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.executors.workunits.info.ExtractMeetings;
import com.rol.sync.vo.SyncTaskVo;

/**
 * @author Anup
 * 
 *         Executes all the work unit defined for the flow All work unit must
 *         extend the SyncWorkUnit
 *
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class WorkUnitsFlowSync {

	@Autowired
	private Validate validate;
	@Autowired
	private ExtractMeetings extractMeetingDet;
	@Autowired
	@Qualifier("CreateMeeting")
	private SyncWorkUnit createSingleOcc;
	@Autowired
	@Qualifier("UpdateMeeting")
	private SyncWorkUnit updateSingleOcc;
	@Autowired
	@Qualifier("DeleteMeeting")
	private SyncWorkUnit deleteSingleOcc;

	public void execute(SyncTaskVo syncTaskVo) throws GsyncException {
		// Get the SyncToke from database

		init(syncTaskVo);
		setSuccesors();
		// Start the chain of execution
		validate.handle();
	}

	private void init(SyncTaskVo syncTaskVo) {
		// Set the flow for creation
		validate.setSyncTaskVo(syncTaskVo);
		extractMeetingDet.setSyncTaskVo(syncTaskVo);

		createSingleOcc.setSyncTaskVo(syncTaskVo);
		updateSingleOcc.setSyncTaskVo(syncTaskVo);
		deleteSingleOcc.setSyncTaskVo(syncTaskVo);

	}

	private void setSuccesors() {
		validate.setSuccessor(extractMeetingDet);
		extractMeetingDet.setSuccessor(createSingleOcc);

		createSingleOcc.setSuccessor(updateSingleOcc);
		updateSingleOcc.setSuccessor(deleteSingleOcc);

	}

}

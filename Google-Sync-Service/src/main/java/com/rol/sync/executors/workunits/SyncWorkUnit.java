package com.rol.sync.executors.workunits;

import java.util.Objects;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.vo.SyncTaskVo;

/**
 * Represents a single task that accounts for the completion of the meeting sync
 * process
 *
 * @author Anup
 */
public abstract class SyncWorkUnit {

	protected abstract void handle() throws GsyncException;
	protected abstract boolean canHandle() throws GsyncException;

	protected SyncWorkUnit successor;
	protected SyncTaskVo syncTaskVo;

	public SyncWorkUnit getSuccessor() {
		return successor;
	}

	public void setSuccessor(SyncWorkUnit successor) {
		this.successor = successor;
	}

	/**
	 * Call the successor if
	 * 1. Instance cannot handle the Scenario
	 * 2. If next successor is available
	 * @param syncTaskVo
	 * @throws GsyncException
	 */
	protected void callSuccesor() throws GsyncException {
		if(Objects.nonNull(successor)) {
			successor.handle();
		}
		
	}

	public SyncTaskVo getSyncTaskVo() {
		return syncTaskVo;
	}

	public void setSyncTaskVo(SyncTaskVo syncTaskVo) {
		this.syncTaskVo = syncTaskVo;
	}

}

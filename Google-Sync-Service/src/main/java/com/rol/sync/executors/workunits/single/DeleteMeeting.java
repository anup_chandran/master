package com.rol.sync.executors.workunits.single;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.executors.workunits.SyncMeeting;
import com.rol.sync.executors.workunits.SyncWorkUnit;
import com.rol.sync.vo.Booking;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;

@Component("DeleteMeeting")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DeleteMeeting extends SyncWorkUnit {

	private Logger logger = LoggerFactory.getLogger(DeleteMeeting.class);

	@Value("${rol_booking_service}")
	private String bookingUrl;

	@Autowired
	@Qualifier("SyncMeetingImpl")
	private SyncMeeting syncOperations;

	private List<GSuiteMeeting> delMeetingLst = new ArrayList<>();

	@Override
	protected void handle() throws GsyncException {
		if (canHandle()) {
			logger.info("In the delete Single occurance , Block");

			delMeetingLst.parallelStream().forEach(t -> {
				try {
					delete(t);
				} catch (GsyncException e) {
					logger.error("Error during parallel processing of Meeting list", t.toString(), e);
				}
			});

		}
		callSuccesor();
	}

	/**
	 *
	 */
	@Override
	protected boolean canHandle() throws GsyncException {

		// Loop the list and return true if there are meetings which needs to be created
		Iterator<GSuiteMeeting> itr = getSyncTaskVo().getMeetings().iterator();
		GSuiteMeeting gMeeting = null;
		while (itr.hasNext()) {
			gMeeting = itr.next();
			if ("cancelled".equals(gMeeting.getStatus())) {
				delMeetingLst.add(gMeeting);
				// Remove the entry from the main list , as it has been processed
				itr.remove();
			}
		}

		return !delMeetingLst.isEmpty();

	}

	/**
	 * @param gMeeting
	 * @throws GsyncException
	 */
	private void delete(GSuiteMeeting gsuiteMeeting) throws GsyncException {
		
		syncOperations.delete(String.valueOf(gsuiteMeeting.getDbBookingObj().getRolBookingId())).join();

	}

}

package com.rol.sync.executors.workunits;

import java.util.concurrent.CompletableFuture;

import org.springframework.http.ResponseEntity;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.vo.Booking;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.rol.ROLBookingRequest;
import com.rol.sync.vo.rol.ROLBookingResponse;

/**
 * @author Anup
 * 
 *         Primary interface isolating the the approach used for executing the
 *         Sync
 * 
 *         1. DB driven approach - Primary - DatabaseDrivenImpl Create - No
 *         record Exists Update/Delete - Record exits Note : DB content is
 *         populated/maintained by Google Sync Application
 * 
 *         2. Meeting Content driven - MeetingContentDrivenImpl Yet to be
 *         Implemented
 * 
 */
public interface SyncMeeting {
	/**
	 * @param gsm
	 * @return
	 * @throws GsyncException
	 */
	public CompletableFuture<ROLBookingResponse> createMeetingReqRol(GSuiteMeeting gsm) throws GsyncException;

	/**
	 * @param rolBookingReq
	 * @return
	 * @throws GsyncException
	 */
	public CompletableFuture<String> updateMeetingReqRol(ROLBookingRequest rolBookingReq) throws GsyncException;

	/**
	 * @param rolBookingId
	 * @return
	 * @throws GsyncException
	 */
	public CompletableFuture<Integer> delete(String rolBookingId) throws GsyncException;

	/**
	 * @throws GsyncException
	 */
	public CompletableFuture<ResponseEntity<String>> createDb(Booking booking) throws GsyncException;

	/**
	 * @param meetingId
	 * @param roomId
	 * @return
	 */
	public CompletableFuture<Booking> bookingInfoDb(String meetingId, String roomId);

}

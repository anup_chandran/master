package com.rol.sync.executors.workunits.single;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.rol.sync.exception.custom.GsyncException;
import com.rol.sync.exception.custom.ParseException;
import com.rol.sync.executors.workunits.SyncMeeting;
import com.rol.sync.executors.workunits.SyncWorkUnit;
import com.rol.sync.util.RolReqJsonGenerator;
import com.rol.sync.vo.Booking;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.rol.ROLBookingResponse;

/**
 * Construct Single occurrence JSON meeting and Post the request to ROL calendar
 * Service
 * 
 * @author Anup
 *
 */
@Component("CreateMeeting")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateMeeting extends SyncWorkUnit {

	@Autowired
	private RolReqJsonGenerator rolReqGen;

	@Value("${rol_booking_service}")
	private String rolBookingUrl;

	@Autowired
	@Qualifier("SyncMeetingImpl")
	private SyncMeeting syncOperations;

	private Logger logger = LoggerFactory.getLogger(CreateMeeting.class);

	private List<GSuiteMeeting> createMeetingLst = new ArrayList<>();

	/**
	 *
	 */
	@Override
	public void handle() throws GsyncException {

		if (canHandle()) {
			logger.info("In the Create Single occurance Meeting !");

			createMeetingLst.parallelStream().forEach(t -> {
				try { // Handle the Sync Operation.
					prepareJson(t);
					makeRequest(t);
				} catch (GsyncException e) {
					logger.error("Error during parallel processing of Meeting list {}", t.toString(), e);
				}
			});

		}

		callSuccesor();
	}

	/**
	 * populate the ROL Response object
	 * 
	 * @throws ParseException
	 * 
	 * @throws GsyncException
	 */
	private void prepareJson(GSuiteMeeting gMeeting) throws ParseException {
		gMeeting.setMeetingDetailsRol(rolReqGen.singleMeetingCreateReq(gMeeting));
	}

	/**
	 * @throws GsyncException
	 */
	@Async
	private void makeRequest(GSuiteMeeting gsm) throws GsyncException {

		ROLBookingResponse rolBookingresp;

		rolBookingresp = syncOperations.createMeetingReqRol(gsm).join();
		// Save it to Db
		Booking booking = new Booking(gsm, rolBookingresp);

		syncOperations.createDb(booking).join();

	}

	/**
	 * Loops through all meetings and
	 */
	@Override
	protected boolean canHandle() throws GsyncException {
		// Loop the list and return true if there are meetings which needs to be created
		Iterator<GSuiteMeeting> itr = getSyncTaskVo().getMeetings().iterator();
		GSuiteMeeting gMeeting = null;
		while (itr.hasNext()) {
			gMeeting = itr.next();
			if ("confirmed".equals(gMeeting.getStatus()) && Objects.isNull(gMeeting.getDbBookingObj())) {
				createMeetingLst.add(gMeeting);
				// Remove the entry from the main list , as it has been processed
				itr.remove();
			}
		}

		return !createMeetingLst.isEmpty();

	}

}

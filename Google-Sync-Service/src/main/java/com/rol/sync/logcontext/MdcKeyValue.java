package com.rol.sync.logcontext;

/**
 * @author Anup
 *
 */
public enum MdcKeyValue {
	
	//Keys 
	
	LOGMSG("trace"),
	RESPONSESTATUS("Response_Status"),
	URL("Url"),
	
	/*
	 * COLLUSIONDETECTIONENABLED("YES"), COLLUSIONDETECTIONDISABLED("YES"),
	 */
	NOTIFICATION("Notification") ;
	 
	//For Type of Notification
	private String desc ;
		
	private MdcKeyValue(String desc) {
		this.desc = desc;
	}
	private MdcKeyValue() {
		this.desc = "";
	}
	public String getDesc() {
		return desc;
	}
	
}

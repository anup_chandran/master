package com.rol.sync.logcontext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

/**
 * @author Anup 
 * Handle all the operation with the MDC
 *
 */
public class MdcUtil {
	
	private MdcUtil() {}

	/**
	 * Add an Single entry to MDC Context
	 * @param key
	 * @param value
	 */
	public static synchronized void addSingleEntry(final String key , final Object value) {
		if(StringUtils.isEmpty(MDC.get(key))) {
			MDC.put(key, String.valueOf(value));
		}else {
			MDC.put(key, MDC.get(key).concat(String.valueOf(value)))  ;
		}
	}
	
	public static synchronized void addLogMsg(final String msg) {
		addSingleEntry(MdcKeyValue.LOGMSG.getDesc(), msg);
	}

	/**
	 * Clears the MDC context
	 */
	public static synchronized void clearMdc() {
		MDC.clear();
	}

}

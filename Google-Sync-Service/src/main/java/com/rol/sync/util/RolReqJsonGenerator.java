package com.rol.sync.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.rol.sync.exception.custom.ParseException;
import com.rol.sync.vo.gmeeting.Attendee;
import com.rol.sync.vo.gmeeting.GSuiteMeeting;
import com.rol.sync.vo.rol.ROLBookingRequest;

/**
 * @author Anup
 *
 */
@Component
public class RolReqJsonGenerator {

	private static Logger logger = LoggerFactory.getLogger(RolReqJsonGenerator.class);

	public ROLBookingRequest singleMeetingCreateReq(final GSuiteMeeting gMeeting)
			throws ParseException {
		return constructBookingReqObj(gMeeting);
	}

	public ROLBookingRequest singleMeetingUpdateReq(final GSuiteMeeting gMeeting) throws ParseException {

		ROLBookingRequest rolBookingReq = constructBookingReqObj(gMeeting);
		rolBookingReq.setBid(String.valueOf(gMeeting.getDbBookingObj().getRolBookingId()));

		return rolBookingReq;
	}

	/**
	 * @param syncTaskVo
	 * @return
	 * @throws ParseException
	 */
	private ROLBookingRequest constructBookingReqObj(final GSuiteMeeting gMeeting)
			throws ParseException {
		// TODO - add the All day booking information , HTML format implementation

		ROLBookingRequest rolBookingReq = new ROLBookingRequest();

		rolBookingReq.setZid(String.valueOf(gMeeting.getMeetingRoom().getRolResourceId()));
		rolBookingReq.setEventId(gMeeting.getId());
		
		rolBookingReq.setOwner("user_1@rolappgroup.onmicrosoft.com");
		rolBookingReq.setAttendees(addAttendees(gMeeting));

		rolBookingReq.setFrom(getEpochTime(gMeeting.getStart().getDateTime(), gMeeting.getMeetingRoom().getTimeZone()));
		rolBookingReq.setUntil(getEpochTime(gMeeting.getEnd().getDateTime(), gMeeting.getMeetingRoom().getTimeZone()));
		rolBookingReq.setLastModified(getEpochTime(gMeeting.getUpdated(), "UTC"));

		rolBookingReq.setDesc(getHtmlBodyContent(gMeeting.getDescription()));
		rolBookingReq.setSubject(gMeeting.getSummary());
		rolBookingReq.setSource("GoogleSync");
		
		if ("private".equals(gMeeting.getVisibility())) {
			rolBookingReq.setPrivate(true);
		}

		return rolBookingReq;
	}
	
	/**
	 * @param makeHtml
	 * @return
	 */
	private String getHtmlBodyContent(final String makeHtml) {
	    String desc = "";
	    if (StringUtils.isBlank(makeHtml)) {
	      return desc;
	    }

	    try {
	      Document doc = Jsoup.parse(makeHtml);
	      Elements select = doc.select("body");
	      desc = select.toString();
	    } catch (Exception e) {
	      logger.error("Jsoup parser error {}", e);
	    }
	    return desc;
	  }

	/**
	 * @param parseDate
	 * @param fromZone
	 * @param toZone
	 * @param dateFormat
	 * @return
	 * @throws org.apache.el.parser.ParseException
	 */

	private long getEpochTime(final String parseDate, final String fromZone) throws ParseException {
		LocalDateTime ldt = null;
		ldt = parseDate(parseDate);

		if (Objects.isNull(ldt)) {
			throw new ParseException("Could not parse with all known formats.");
		}

		ZoneId fromZoneId = ZoneId.of(fromZone);
		ZonedDateTime toZonedDateTime = ldt.atZone(fromZoneId);
		ZoneId toZoneId = ZoneId.of("UTC");
		ZonedDateTime fromZonedDateTime = toZonedDateTime.withZoneSameInstant(toZoneId);
		Date date = Date.from(fromZonedDateTime.toInstant());
		return date.getTime();
	}

	/**
	 * @param dateTimeStr
	 * @return
	 * @throws DateTimeParseException
	 */
	private LocalDateTime parseDate(final String dateTimeStr) {
		LocalDateTime ldt = null;

		for (DateTimeFormatter dateTimeFormatter : formatterList()) {
			try {
				ldt = LocalDateTime.parse(dateTimeStr, dateTimeFormatter);
			} catch (DateTimeParseException dte) {
				logger.debug("Could not parse with {}", dateTimeFormatter);
			}

			if (Objects.nonNull(ldt)) {
				break;
			}
		}

		return ldt;
	}

	/**
	 * FormatterOne - > "2019-11-26T21:30:00.000+05:30"
	 * 
	 * @return
	 */
	private List<DateTimeFormatter> formatterList() {
		List<DateTimeFormatter> lstFormatters = new ArrayList<>();

		DateTimeFormatter formatterOne = new DateTimeFormatterBuilder().appendPattern("yyyy").appendLiteral('-')
				.appendValue(ChronoField.MONTH_OF_YEAR).appendLiteral('-').appendValue(ChronoField.DAY_OF_MONTH)
				.appendPattern("'T'").appendValue(ChronoField.HOUR_OF_DAY).appendLiteral(':')
				.appendValue(ChronoField.MINUTE_OF_HOUR).appendLiteral(':').appendValue(ChronoField.SECOND_OF_MINUTE)
				.appendLiteral('.').appendValue(ChronoField.MILLI_OF_SECOND).appendZoneId().parseLenient()
				.parseCaseInsensitive().toFormatter();

		lstFormatters.add(formatterOne);

		return Collections.unmodifiableList(lstFormatters);
	}

	private List<com.rol.sync.vo.rol.Attendee> addAttendees(GSuiteMeeting gSuiteMeeting) {

		List<com.rol.sync.vo.rol.Attendee> lstAttendee = new ArrayList<>();
		com.rol.sync.vo.rol.Attendee at1 = null;

		for (Attendee attendees : gSuiteMeeting.getAttendees()) {

			if (Objects.nonNull(attendees.getResource()) && attendees.getResource().booleanValue()) {
				continue;
			}
			at1 = new com.rol.sync.vo.rol.Attendee();
			at1.setUsername(attendees.getEmail());
			at1.setRequired(true);

			if ("needsAction".equals(attendees.getResponseStatus())) {
				at1.setStatus("WAITING");
			} else {
				at1.setStatus(attendees.getResponseStatus().toUpperCase());
			}

			at1.setGuests(0L);

			lstAttendee.add(at1);
		}

		return lstAttendee;
	}

}

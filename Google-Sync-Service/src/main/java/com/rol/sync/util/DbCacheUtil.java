package com.rol.sync.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * Temporarily class for database approach , 
 * will be replaced with db service call
 * @author Anup
 *
 */
@Component
public class DbCacheUtil {
	
	private Map<String,Long> dbCache = new HashMap<>();

	public Map<String, Long> getDbCache() {
		return dbCache;
	}

}

package com.rol.sync.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rol.sync.exception.custom.ParseException;
import com.rol.sync.vo.TokenErrorBean;

/**
 * @author Anup
 *
 */
@ControllerAdvice
@Component
public class ExceptionAdvise {
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionAdvise.class.getSimpleName());
	
	private static final String APP_VERSION = "1.0" ;
	
	/**
	 * Method to handle Exception , un-handled Exception
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(Exception ex) {
		TokenErrorBean errorBean = new TokenErrorBean();
		errorBean.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
		errorBean.setAppVersion(APP_VERSION);
		errorBean.setErrorMessage(ex.getMessage());
		
		logger.error("UnCaught Exception !!! ", ex);
		
	}
	
	
	@ExceptionHandler(ParseException.class)
	public void exceptionHandler(ParseException ex) {
		TokenErrorBean errorBean = new TokenErrorBean();
		errorBean.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
		errorBean.setAppVersion(APP_VERSION);
		errorBean.setErrorMessage(ex.getMessage());
		
		logger.error("UnCaught Exception !!! ", ex);
	}
	
	

}

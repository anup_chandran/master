package com.rol.sync.exception.custom;

/**
 * @author Anup
 *  Custom exception , base
 *
 */
public class ParseException extends GsyncException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6578647644483744420L;

	public ParseException(String errorMsg) {
		super.getTokenErrorBean().setErrorMessage(errorMsg);
	}

}

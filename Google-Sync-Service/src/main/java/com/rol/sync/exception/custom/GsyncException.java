package com.rol.sync.exception.custom;

import com.rol.sync.vo.TokenErrorBean;

/**
 * @author Anup
 *  Custom exception , base
 *
 */
public class GsyncException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1217026581445748942L;
	private final transient TokenErrorBean tokenErrorBean = new TokenErrorBean() ;
	
	
	
	public TokenErrorBean getTokenErrorBean() {
		return this.tokenErrorBean ;
	}

}

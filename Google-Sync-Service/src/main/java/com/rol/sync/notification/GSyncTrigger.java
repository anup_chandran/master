package com.rol.sync.notification;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Component;

import com.rol.sync.conf.GsyncTaskPool;

@Component
public class GSyncTrigger implements Trigger {
	@Autowired
	private GsyncTaskPool gSyncTaskPool;

	@Override
	public Date nextExecutionTime(TriggerContext triggerContext) {

		Logger logger = LoggerFactory.getLogger(this.getClass());

		if (gSyncTaskPool.areAllThreadActive()) {
			Date date = new Date(System.currentTimeMillis() + 10000);
			if(logger.isInfoEnabled()) {
				logger.info("Not acccepting ! {}",date);
			}

			return date;
		}

		return new Date(System.currentTimeMillis() + 1000);
		
	}

}

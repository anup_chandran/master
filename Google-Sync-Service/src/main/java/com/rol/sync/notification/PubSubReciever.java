package com.rol.sync.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubMessageSource;
import org.springframework.cloud.gcp.pubsub.support.AcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;

import com.rol.sync.conf.SyncTaskQueue;

@Configuration
public class PubSubReciever {

	private static final Logger logger = LoggerFactory.getLogger(PubSubReciever.class);

	@Autowired
	private SyncTaskQueue processQueue;

	@Bean
	@InboundChannelAdapter(channel = "pubsubInputChannel", poller = @Poller(trigger = "GSyncTrigger"))
	public PubSubMessageSource pubsubAdapter(PubSubTemplate pubSubTemplate) {
		PubSubMessageSource messageSource = new PubSubMessageSource(pubSubTemplate, "RolCalServiceSub");
		messageSource.setMaxFetchSize(5);
		messageSource.setAckMode(AckMode.MANUAL);
		messageSource.setPayloadType(String.class);
		return messageSource;
	}

	@ServiceActivator(inputChannel = "pubsubInputChannel")
	public void messageReceiver(String payload,
			@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) AcknowledgeablePubsubMessage message) {

		processQueue.add(payload);
		
		if (logger.isInfoEnabled()) {
			logger.info(payload);
		}

		message.ack();
	}

}

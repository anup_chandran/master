package com.rol.gauth.authorize.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import com.rol.gauth.authorize.exception.custom.RolGCalSyncException;

/**
 * @author Anup Holds all the files associated with the service account
 * 
 */
@Component
@Scope("singleton")
public class ServiceAccountFilesUtil {
	@Autowired
	private JsonParsedMapUtil parsedMapJson;
	@Autowired
	private ResourceLoader fileLoader;

	private File p12FileObj;
	
	@Value("${service-account-p12-File}")
	private String p12File ;
	@Value("${service-account-p12-JSON}")
	private String jsonFile ;
	
	private String serviceAccountKey = "client_email";

	/**
	 * Returns the p12 file object
	 * 
	 * @throws RolGCalSyncException
	 */
	public File getP12File() throws RolGCalSyncException {
		if (Objects.isNull(p12FileObj)) {
			initialize();
		}

		return p12FileObj;
	}

	/**
	 * @return configured service account name
	 * @throws RolGCalSyncException
	 */
	public String getServiceAccountName() throws RolGCalSyncException {
		return getValue(serviceAccountKey);
	}

	/**
	 * @param key
	 * @return
	 * @throws RolGCalSyncException
	 */
	private String getValue(String key) throws RolGCalSyncException {
		if (parsedMapJson.loadProperties()) {
			initialize();
		}

		return parsedMapJson.getValue(key);
	}

	/**
	 * Will load the p12 and JSON
	 * 
	 * @throws RolGCalSyncException
	 */
	private void initialize() throws RolGCalSyncException {
		// Load the JSON file
		parsedMapJson.parseJson(getFileObj(jsonFile));
		// Load the p12 file
		p12FileObj = getFileObj(p12File);
	}

	/**
	 * @param filePath
	 * @return
	 * @throws RolGCalSyncException
	 */
	private File getFileObj(final String filePath) throws RolGCalSyncException {
		Resource resource = fileLoader.getResource(filePath);
		File file = null ;
		if (!resource.exists()) {
			throw getAuthTokenErrObject(new FileNotFoundException("Could not find the".concat(filePath)));
		}
		try {
			file = File.createTempFile("serviceAcnt", "p12") ;
			FileCopyUtils.copy(resource.getInputStream().readAllBytes(), file);
		} catch (IOException e) {
			throw getAuthTokenErrObject(new FileNotFoundException("Could not find the".concat(filePath)));
		}

		return file;
	}

	/**
	 * @return
	 */
	private RolGCalSyncException getAuthTokenErrObject(Exception e) {
		RolGCalSyncException authTokenException = new RolGCalSyncException();
		authTokenException.setStackTrace(e.getStackTrace());
		authTokenException.getTokenErrorBean().setErrorMessage(
				"Exception during obtaining the Service Account token , Could not read configuration File (p12) file");

		return authTokenException;
	}

}

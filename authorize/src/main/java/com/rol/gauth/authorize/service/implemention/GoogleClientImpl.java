package com.rol.gauth.authorize.service.implemention;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.rol.gauth.authorize.exception.custom.RolGCalSyncException;
import com.rol.gauth.authorize.service.TokenService;
import com.rol.gauth.authorize.utility.AuthTokenCache;
import com.rol.gauth.authorize.utility.ServiceAccountFilesUtil;
import com.rol.gauth.authorize.vo.AuthTokenBean;

/**
 * @author Anup
 * Gets the Token using Thread-safe Google-specific implementation of the OAuth 2.0
 */
@Component("GoogleClientImpl")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GoogleClientImpl implements TokenService {

	@Autowired
	private AuthTokenCache authTokenCache;
	@Autowired
	private ServiceAccountFilesUtil serviceAccount;

	@Override
	public AuthTokenBean getTokenForSeviceAccount() throws RolGCalSyncException {

		AuthTokenBean authToken = authTokenCache.getFromCache(serviceAccount.getServiceAccountName());
		if (Objects.nonNull(authToken)) { // Return token if available
			return authToken;
		}

		authToken = extractToken();
		// Update the cache
		authTokenCache.addAuthToken(serviceAccount.getServiceAccountName(), authToken);

		// Return the token from the cache
		return authToken;
	}

	@Override
	public AuthTokenBean getTokenForSeviceAccount(boolean refreshToken) throws RolGCalSyncException {
		AuthTokenBean authToken = extractToken();
		// Update the cache
		authTokenCache.updateCache(serviceAccount.getServiceAccountName(), authToken);

		return authToken;
	}

	@Override
	public AuthTokenBean getImpersonateToken(final String userName) throws RolGCalSyncException {
		AuthTokenBean authToken = authTokenCache.getFromCache(userName);
		if (Objects.nonNull(authToken)) { // Return token if available
			return authToken;
		}

		authToken = extractToken(userName);
		// Update the cache
		authTokenCache.addAuthToken(userName, authToken);

		// Return the token from the cache
		return authToken;
	}

	@Override
	public AuthTokenBean getImpersonateToken(String userName, boolean refreshToken) throws RolGCalSyncException {

		AuthTokenBean authToken = extractToken(userName);
		// Update the cache
		authTokenCache.updateCache(userName, authToken);

		return authToken;
	}

	/**
	 * @return
	 * @throws RolGCalSyncException
	 */
	private AuthTokenBean extractToken() throws RolGCalSyncException {
		AuthTokenBean authTknBean = null;
		try {
			GoogleCredential credential = createCredentialForServiceAccount();
			credential.refreshToken();// Get the Token
			authTknBean = new AuthTokenBean(credential);
		} catch (GeneralSecurityException | IOException e) {
			throw getAuthTokenErrObject(e);
		}
		return authTknBean;
	}

	/**
	 * @param userName
	 * @return
	 * @throws RolGCalSyncException
	 */
	private AuthTokenBean extractToken(final String userName) throws RolGCalSyncException {
		AuthTokenBean authTknBean = null;
		try {
			GoogleCredential credential = createCredentialForServiceAccountImpersonateUser(userName);
			credential.refreshToken();// Get the Token
			authTknBean = new AuthTokenBean(credential);
		} catch (GeneralSecurityException | IOException e) {
			throw getAuthTokenErrObject(e);
		}
		return authTknBean;
	}

	/**
	 * @return
	 */
	private RolGCalSyncException getAuthTokenErrObject(Exception e) {
		RolGCalSyncException authTokenException = new RolGCalSyncException();
		authTokenException.setStackTrace(e.getStackTrace());
		authTokenException.getTokenErrorBean()
				.setErrorMessage("Exception during obtaining the Service Account token !");

		return authTokenException;
	}

	/**
	 * @param transport
	 * @param jsonFactory
	 * @param serviceAccountId
	 * @param serviceAccountScopes
	 * @param p12File
	 * @param serviceAccountUser
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws RolGCalSyncException
	 */
	private GoogleCredential createCredentialForServiceAccountImpersonateUser(String serviceAccountUser)
			throws GeneralSecurityException, IOException, RolGCalSyncException {

		return new GoogleCredential.Builder().setTransport(new NetHttpTransport()).setJsonFactory(new JacksonFactory())
				.setServiceAccountId(serviceAccount.getServiceAccountName())
				.setServiceAccountScopes(Arrays.asList(CalendarScopes.CALENDAR_EVENTS_READONLY))
				.setServiceAccountPrivateKeyFromP12File(serviceAccount.getP12File())
				.setServiceAccountUser(serviceAccountUser).build();
	}

	/**
	 * @param transport
	 * @param jsonFactory
	 * @param serviceAccountId
	 * @param serviceAccountScopes
	 * @param p12File
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws RolGCalSyncException
	 */
	private GoogleCredential createCredentialForServiceAccount()
			throws GeneralSecurityException, IOException, RolGCalSyncException {

		return new GoogleCredential.Builder().setTransport(new NetHttpTransport()).setJsonFactory(new JacksonFactory())
				.setServiceAccountId(serviceAccount.getServiceAccountName())
				.setServiceAccountScopes(Arrays.asList(CalendarScopes.CALENDAR_EVENTS_READONLY))
				.setServiceAccountPrivateKeyFromP12File(serviceAccount.getP12File()).build();
	}

}

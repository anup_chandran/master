package com.rol.gauth.authorize.utility;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.map.LRUMap;

import com.rol.gauth.authorize.exception.custom.RolGCalSyncException;
import com.rol.gauth.authorize.vo.AuthTokenBean;

/**
 * @author Anup
 *
 * Simple cache for Google Auth token from google,
 * 	Reset each day at 10 PM
 *  Max allowed of 5000 entries , 
 *  
 */
public class AuthTokenCache {
	private  Map<String, AuthTokenBean> authTokenCacheMap ;
	
	
	public AuthTokenCache() {
		setLruMap() ;
	}
	
	private void setLruMap() {
		//Setting the Max size to be 5000
		authTokenCacheMap = new LRUMap<>(5000);
		//Make the Map Thread Safe
		authTokenCacheMap = Collections.synchronizedMap(authTokenCacheMap);
	}
	
	/**
	 * Cleans up map every 12 hours
	 */
	public void resetMap() {
		authTokenCacheMap.clear();
	}
	/**
	 * Update the Cache
	 * @param userName
	 * @throws RolGCalSyncException
	 */
	public void updateCache(final String userName, final AuthTokenBean authTknBean) {
		
		authTokenCacheMap.put(userName, authTknBean);
		
	}
	
	public boolean isKeyPresent(final String userName) {
		return Objects.nonNull(authTokenCacheMap.get(userName)) ;
	}
	
	public AuthTokenBean getFromCache(final String userName) {
		return authTokenCacheMap.get(userName) ;
	}
	
	/**
	 * Adds the token if not present
	 */
	public void addAuthToken(final String userName,final AuthTokenBean authTknBean) {
		updateCache(userName,authTknBean) ;
	}

}

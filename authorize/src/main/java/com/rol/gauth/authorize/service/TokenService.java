package com.rol.gauth.authorize.service;

import com.rol.gauth.authorize.exception.custom.RolGCalSyncException;
import com.rol.gauth.authorize.vo.AuthTokenBean;

/**
 * @author Anup
 * Service gets the token from the GSuite Rest services using  
 * Thread-safe Google-specific implementation of the OAuth 2.0
 */
public interface TokenService {
	
	/**
	 * Get token for the service ROL Google Sync service Account from cache / google
	 * @return AuthTokenBean 
	 */
	public AuthTokenBean getTokenForSeviceAccount() throws RolGCalSyncException;
	
	/**
	 * Get token for the service ROL Google Sync service Account from Google Client and update cache
	 * 
	 * @return AuthTokenBean 
	 */
	public AuthTokenBean getTokenForSeviceAccount(final boolean refreshToken) throws RolGCalSyncException;
	
	/**
	 * Get the impersonated Token extracting from Cache / Google Client and updates the
	 * cache Returns the impersonated Token
	 * @return AuthTokenBean
	 */
	public AuthTokenBean getImpersonateToken(final String userName) throws RolGCalSyncException;
	
	/**
	 * Get the impersonated Token extracting from Google Client and updates the
	 * cache Returns
	 * @return AuthTokenBean
	 */
	public AuthTokenBean getImpersonateToken(final String userName,final boolean refreshToken) throws RolGCalSyncException;

}

package com.rol.gauth.authorize.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rol.gauth.authorize.utility.AuthTokenCache;

@Configuration
public class Config {
	
	/**
	 * Jackson parser object Implementation 
	 * @return Jackson Object for parsing the JSON
	 */
	@Bean
	public ObjectMapper objectMapper() {

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

		return objectMapper;
	}
	
	/**
	 * Google parser object implementation
	 * @return GSON object for parsing the JSON
	 */
	@Bean
	public Gson getGson() {
		GsonBuilder gsonBuilder = new GsonBuilder() ;
		return gsonBuilder.create();
	}
	
	/**
	 * @return
	 */
	@Bean
	public AuthTokenCache getAuthTokenCache() {
		return new AuthTokenCache();
	}

}

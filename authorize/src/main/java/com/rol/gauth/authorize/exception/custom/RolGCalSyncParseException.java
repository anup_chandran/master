package com.rol.gauth.authorize.exception.custom;

/**
 * @author Anup
 *
 */
public class RolGCalSyncParseException extends RolGCalSyncException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5363583496236438920L;
	
	public RolGCalSyncParseException(String errorMessage) {
		super.getTokenErrorBean().setErrorMessage(errorMessage);
	}

}

package com.rol.gauth.authorize.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.rol.gauth.authorize.utility.AuthTokenCache;

@Configuration
@EnableScheduling
public class CacheToken {
	private Logger logger = LoggerFactory.getLogger(CacheToken.class) ;
	@Autowired
	private AuthTokenCache authTokenCache ;

	/**
	 * Reset the cache token each day
	 */
	@Scheduled(cron = "${reset_auth_cache}")
	public void renewAuthTokenCache() {
		logger.info("Clearing the Cache Token Map");
		authTokenCache.resetMap();
	}
	
}

package com.rol.gauth.authorize.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rol.gauth.authorize.exception.custom.RolGCalSyncException;
import com.rol.gauth.authorize.exception.custom.RolGCalSyncParseException;

/**
 * @author Anup
 * Utility class to extract json value using Jackson
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JsonParsedMapUtil {

	private Map<String, String> parsedMap;
	@Autowired
	private ObjectMapper jacksonMapper;

	/**
	 * Parse the JSON String and store the value in the Map, values can be retrieved
	 * using the getValue method
	 * 
	 * @param jsonStr
	 * @return
	 * @throws RolGCalSyncException
	 */
	@SuppressWarnings("unchecked")
	public void parseJson(String jsonStr) throws RolGCalSyncException {
		try {
			parsedMap = jacksonMapper.readValue(jsonStr, Map.class);
		} catch (IOException e) {
			throw new RolGCalSyncParseException("Could not parse");
		}
	}

	/**
	 * @param jsonFile
	 * @throws RolGCalSyncException
	 */
	@SuppressWarnings("unchecked")
	public void parseJson(File jsonFile) throws RolGCalSyncException {
		try (FileInputStream fileInputStream = new FileInputStream(jsonFile)) {
			parsedMap = jacksonMapper.readValue(fileInputStream.readAllBytes(), Map.class);
		} catch (IOException e) {
			throw new RolGCalSyncParseException("Could not parse");
		}
	}

	/**
	 * Returns the value of the key , Blank if key does not exist
	 * @param key
	 * @return
	 */
	public String getValue(final String key) {
		return getParsedMap().getOrDefault(key, Strings.EMPTY);
	}
	
	/**
	 * @return
	 */
	public boolean loadProperties() {
		return Objects.isNull(parsedMap) || parsedMap.isEmpty() ;
	}

	/**
	 * @return
	 */
	private Map<String, String> getParsedMap() {
		return parsedMap;
	}

}

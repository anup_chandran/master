package com.rol.gauth.authorize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GAuthTokenService extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(GAuthTokenService.class, args);
	}

}

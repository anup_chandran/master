package com.rol.gauth.authorize.vo;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

/**
 * @author Anup
 * Bean to hold the values of the tokens retrieved from the Google OAuth2 
 */
public class AuthTokenBean {
	private String authToken ;
	private String refreshToken ;
	private Long exprirationTimeMilliSeconds ;
	private Long expiresInSeconds ;
	
	private String serviceAccount;
	private String impersonatedUser ;
	
	/**
	 * Will set all the parameters available in GoogleCredential object to bean
	 * Typically called after getting the response from 
	 * Thread-safe Google-specific implementation of the OAuth 2.0 helper for accessing protectedresources using an access token
	 * @param credential
	 */
	public AuthTokenBean(GoogleCredential credential) {
		this.authToken = credential.getAccessToken() ;
		this.refreshToken = credential.getRefreshToken() ;
		this.exprirationTimeMilliSeconds = credential.getExpirationTimeMilliseconds() ;
		this.expiresInSeconds = credential.getExpiresInSeconds() ;
		//User data 
		this.serviceAccount = credential.getServiceAccountId() ;
		this.impersonatedUser = credential.getServiceAccountUser();
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Long getExprirationTimeMilliSeconds() {
		return exprirationTimeMilliSeconds;
	}

	public void setExprirationTimeMilliSeconds(Long exprirationTimeMilliSeconds) {
		this.exprirationTimeMilliSeconds = exprirationTimeMilliSeconds;
	}

	public Long getExpiresInSeconds() {
		return expiresInSeconds;
	}

	public void setExpiresInSeconds(Long expiresInSeconds) {
		this.expiresInSeconds = expiresInSeconds;
	}

	public String getServiceAccount() {
		return serviceAccount;
	}

	public void setServiceAccount(String serviceAccount) {
		this.serviceAccount = serviceAccount;
	}

	public String getImpersonatedUser() {
		return impersonatedUser;
	}

	public void setImpersonatedUser(String impersonatedUser) {
		this.impersonatedUser = impersonatedUser;
	}
	
	
	

}

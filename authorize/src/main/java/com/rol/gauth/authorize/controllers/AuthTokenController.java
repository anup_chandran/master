package com.rol.gauth.authorize.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rol.gauth.authorize.exception.custom.RolGCalSyncException;
import com.rol.gauth.authorize.service.TokenService;
import com.rol.gauth.authorize.vo.AuthTokenBean;
import com.rol.gauth.logcontext.MdcKeyValue;
import com.rol.gauth.logcontext.MdcUtil;

/**
 * @author Anup
 *
 */
@RestController
public class AuthTokenController {

	@Autowired
	@Qualifier("GoogleClientImpl")
	private TokenService tokenService;

	private static final HttpStatus STATUS = HttpStatus.valueOf(HttpStatus.OK.value());

	

	/**
	 * @param serviceAccountName
	 * @return
	 * @throws AuthorizeTokenException
	 */
	@GetMapping(value = "/serviceaccount")
	public ResponseEntity<AuthTokenBean> obtainServiceToken() throws RolGCalSyncException {
		
		AuthTokenBean authBean = tokenService.getTokenForSeviceAccount() ;
		
		return new ResponseEntity<>(authBean, STATUS);
	}
	
	/**
	 * @param serviceAccountName
	 * @return
	 * @throws AuthorizeTokenException
	 */
	@GetMapping(value = "/serviceaccount/refresh")
	public ResponseEntity<AuthTokenBean> obtainServiceTokenRefresh() throws RolGCalSyncException {
		MdcUtil.addSingleEntry(MdcKeyValue.REQUEST_URL.name(),"/serviceaccount/refresh");
		
		AuthTokenBean authBean = tokenService.getTokenForSeviceAccount(true) ;
		
		return new ResponseEntity<>(authBean, STATUS);
	}

	/**
	 * @param serviceAccountName
	 * @param userName
	 * @return
	 * @throws AuthorizeTokenException
	 */
	@GetMapping(value = "/user")
	public ResponseEntity<AuthTokenBean> obtainImpersonateToken(@RequestParam(required = true) String userName)
			throws RolGCalSyncException {
		MdcUtil.addSingleEntry(MdcKeyValue.REQUEST_URL.name(),"/user");
		AuthTokenBean authToken =  tokenService.getImpersonateToken(userName) ;
		//Clear the 
		return new ResponseEntity<>(authToken, STATUS);

	}
	
	/**
	 * @param serviceAccountName
	 * @param userName
	 * @return
	 * @throws AuthorizeTokenException
	 */
	@GetMapping(value = "/user/refresh")
	public ResponseEntity<AuthTokenBean> obtainImpersonateTokenRefresh(@RequestParam(required = true) String userName)
			throws RolGCalSyncException {
		MdcUtil.addSingleEntry(MdcKeyValue.REQUEST_URL.name(),"/user/refresh");
		return new ResponseEntity<>(tokenService.getImpersonateToken(userName,true), STATUS);

	}
	

}

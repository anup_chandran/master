package com.rol.gauth.authorize.exception.custom;

import com.rol.gauth.authorize.vo.TokenErrorBean;

/**
 * @author Anup
 *
 */
public class RolGCalSyncException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1217026581445748942L;
	private final transient TokenErrorBean tokenErrorBean = new TokenErrorBean() ;
	
	
	
	public TokenErrorBean getTokenErrorBean() {
		return this.tokenErrorBean ;
	}

}

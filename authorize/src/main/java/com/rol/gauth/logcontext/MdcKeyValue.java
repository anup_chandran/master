package com.rol.gauth.logcontext;

/**
 * @author Anup
 *
 */
public enum MdcKeyValue {
	
	//Keys 
	REQUEST_URL("RequestUrl"),
	
	ROL_ID("RolId"),
	O365ID_RESOURCE("O365_RoomId"),
	O365IDMEETINGUSER("O365_User_MeetingId"),
	O365IDOCCURANCE("O365_OccuranceId"),
	SYNCOPERATION("Sync_Operation"),
	ORIGIN("ORIGIN"),
	REASON("Reason"),
	COLLUSIONDETECTION("Collusion_Detection"),
	COLLUSIONDETECTION_RESULT("Collusion_Detection_Result"),
	ESYNCOPERATION("ESync_Operation"),
	JSONREQUEST("JSON_Request"),
	ESYNCINITIATED("Esync_Initiated"),
	MEETINGTYPE("Meeting_Type"),
	SHAREDROOM("Booking_Room"),
	SHAREDROOMS("Bookings_Rooms"),
	MARKEDFOREXIT("Request_Marked_For_Exit"),
	SAMEMEETING("Same_User_Meeting"),
	DIFFERENTMEETING("Different_User_Meeting"),
	METHODCALLED("Direct_Method_Call"),
	ROLTRIGGERJSON("Rol_Trigger_Json"),
	O365TRIGGERJSON("O365_Notification"),
	URL("Url"),
	REQUESTEXIT("Request_Processing_Terminated"),
	LOGMSG("log_msg"),
	
	//values
	YES("Yes"),
	NO("No"),
	CREATE("Create"),
	UPDATE("Update"),
	DELETE("Delete"),
	O365("O365"),
	ROL("Rol"),
	SINGLE("Single"),
	MULTIPLEROOM("MultipleRoom"),
	RECURRANCE("Recurrance"),
	ROLTRIGGERED("Processing the status update from ROL"),
	O365TRIGGERED("Processing the Notification from Exchange"),
	
	
	/*
	 * COLLUSIONDETECTIONENABLED("YES"), COLLUSIONDETECTIONDISABLED("YES"),
	 */
	RESOURCEEVENTID("Room event Id"),
	DELETEO365URL("Delete Url for O365"),
	DELETERESPONSESTATUS("Result for delete call"),
	REGISTERED_DELETE_ROL("Got status Delete from ROL"),
	RESPONSESTATUS("Response_Status"),
	
	//For Sync- ReportStatus Table 
	/*
	 * O365_REGISTER_CREATE("Create notification registered from O365"),
	 * O365_REGISTER_UPDATE("Update notification registered from O365"),
	 * O365_REGISTER_DELETE("Delete notification registered from O365"),
	 * O365_PROCESS("Processing notification from O365"),
	 * O365_COMPLETE("Completed notification process from O365"),
	 * ROL_REGISTER_CREATE("Create Notification registered from ROL for O365"),
	 * ROL_REGISTER_UPDATE("Update Notification registered from ROL for O365"),
	 * ROL_PROCESS("Processing notification from ROL for O365"), */
	 ROL_COMPLETE("Completed notification process from ROL for O365") ;
	 
	//For Type of Notification
	private String desc ;
		
	private MdcKeyValue(String desc) {
		this.desc = desc;
	}
	private MdcKeyValue() {
		this.desc = "";
	}
	public String getDesc() {
		return desc;
	}
	
}

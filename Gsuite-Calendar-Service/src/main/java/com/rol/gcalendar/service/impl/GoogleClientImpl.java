package com.rol.gcalendar.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rol.gcalendar.cmd.EventDetails;
import com.rol.gcalendar.cmd.EventsList;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.service.MeetingService;

@Service("GoogleClient")
public class GoogleClientImpl implements MeetingService {

	@Autowired
	private EventDetails gsuiteClient;
	@Autowired
	private EventsList gSuiteClientEvents ;

	@Override
	public String getMeetingDetails(String watchJson) throws GcalException {
		return gsuiteClient.execute(watchJson);
	}

	@Override
	public String getEventsLatestSyncTkn(String gResourceId) throws GcalException, IOException {
		
		return gSuiteClientEvents.extractMeetingLatestSyncToken(gResourceId);
	}

	

}

package com.rol.gcalendar.service;

import java.io.IOException;

import com.rol.gcalendar.exception.custom.GcalException;

/**
 * @author Anup
 *
 */
public interface MeetingService {
	
	/**
	 * Extracts the meeting details using the meetingID
	 * @param meetingId
	 * @return
	 */
	public String getMeetingDetails(String meetingId) throws GcalException;
	
	public String getEventsLatestSyncTkn(String gResourceId) throws GcalException, IOException ;

}

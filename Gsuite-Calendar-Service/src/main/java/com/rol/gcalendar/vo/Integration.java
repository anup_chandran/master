package com.rol.gcalendar.vo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Integration {

	private Long integrationId;

	private Long cid;

	private Long realm;

	private Long lastModified;

	private String active;

	private String defaultUser;
	
	public Integration() {
		
	}

	public Integration(ResultSet rs, int rowNum) throws SQLException {
		if(Objects.nonNull(rowNum)) {
			setIntegrationId(rs.getLong("integrationid"));
			setCid(rs.getLong("cid"));
			setRealm(rs.getLong("realm"));
			setLastModified(rs.getLong("lastmodified"));
			setActive(rs.getString("active"));
			setDefaultUser(rs.getString("defaultuser"));
		}

	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public Long getRealm() {
		return realm;
	}

	public void setRealm(Long realm) {
		this.realm = realm;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getDefaultUser() {
		return defaultUser;
	}

	public void setDefaultUser(String defaultUser) {
		this.defaultUser = defaultUser;
	}

	public Long getIntegrationId() {
		return integrationId;
	}

	public void setIntegrationId(Long integrationId) {
		this.integrationId = integrationId;
	}

}

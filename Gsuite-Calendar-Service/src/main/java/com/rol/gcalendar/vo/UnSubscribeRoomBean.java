package com.rol.gcalendar.vo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "resourceId", "defaultuser" })
public class UnSubscribeRoomBean {

	@JsonProperty("id")
	private String id;
	@JsonProperty("resourceId")
	private String resourceId;
	@JsonProperty("defaultuser")
	private String defaultuser;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("resourceId")
	public String getResourceId() {
		return resourceId;
	}

	@JsonProperty("resourceId")
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@JsonProperty("defaultuser")
	public String getDefaultuser() {
		return defaultuser;
	}

	@JsonProperty("defaultuser")
	public void setDefaultuser(String defaultuser) {
		this.defaultuser = defaultuser;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
package com.rol.gcalendar.vo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "email", "organizer", "responseStatus", "displayName", "self", "resource" })
public class Attendee {

	@JsonProperty("email")
	private String email;
	@JsonProperty("organizer")
	private Boolean organizer;
	@JsonProperty("responseStatus")
	private String responseStatus;
	@JsonProperty("displayName")
	private String displayName;
	@JsonProperty("self")
	private Boolean self;
	@JsonProperty("resource")
	private Boolean resource;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("organizer")
	public Boolean getOrganizer() {
		return organizer;
	}

	@JsonProperty("organizer")
	public void setOrganizer(Boolean organizer) {
		this.organizer = organizer;
	}

	@JsonProperty("responseStatus")
	public String getResponseStatus() {
		return responseStatus;
	}

	@JsonProperty("responseStatus")
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	@JsonProperty("displayName")
	public String getDisplayName() {
		return displayName;
	}

	@JsonProperty("displayName")
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@JsonProperty("self")
	public Boolean getSelf() {
		return self;
	}

	@JsonProperty("self")
	public void setSelf(Boolean self) {
		this.self = self;
	}

	@JsonProperty("resource")
	public Boolean getResource() {
		return resource;
	}

	@JsonProperty("resource")
	public void setResource(Boolean resource) {
		this.resource = resource;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
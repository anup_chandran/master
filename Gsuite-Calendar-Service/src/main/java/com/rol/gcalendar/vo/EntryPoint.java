package com.rol.gcalendar.vo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "entryPointType", "uri", "label", "regionCode", "pin" })
public class EntryPoint {

	@JsonProperty("entryPointType")
	private String entryPointType;
	@JsonProperty("uri")
	private String uri;
	@JsonProperty("label")
	private String label;
	@JsonProperty("regionCode")
	private String regionCode;
	@JsonProperty("pin")
	private String pin;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("entryPointType")
	public String getEntryPointType() {
		return entryPointType;
	}

	@JsonProperty("entryPointType")
	public void setEntryPointType(String entryPointType) {
		this.entryPointType = entryPointType;
	}

	@JsonProperty("uri")
	public String getUri() {
		return uri;
	}

	@JsonProperty("uri")
	public void setUri(String uri) {
		this.uri = uri;
	}

	@JsonProperty("label")
	public String getLabel() {
		return label;
	}

	@JsonProperty("label")
	public void setLabel(String label) {
		this.label = label;
	}

	@JsonProperty("regionCode")
	public String getRegionCode() {
		return regionCode;
	}

	@JsonProperty("regionCode")
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	@JsonProperty("pin")
	public String getPin() {
		return pin;
	}

	@JsonProperty("pin")
	public void setPin(String pin) {
		this.pin = pin;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
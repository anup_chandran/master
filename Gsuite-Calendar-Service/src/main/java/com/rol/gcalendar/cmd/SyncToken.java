package com.rol.gcalendar.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.rol.gcalendar.logcontext.MdcUtil;
import com.rol.gcalendar.vo.SubscribeRoomResponse;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SyncToken {

	@Autowired
	private SyncTokenExtractor syncTokenExtractor;
	

	Logger logger = LoggerFactory.getLogger(SyncToken.class);

	public void process(final SubscribeRoomResponse subsRoomResp) {

//		Runnable executeProcess = () -> {
			try {
				// Execute the flow for the Collision
				syncTokenExtractor.extract(subsRoomResp);

			} catch (Exception e) {// Catch any exception that occurred during execution and log the report

				logger.error("Error during o365 retrieval ", e);
				Thread.currentThread().interrupt();
			} finally {
				// Clear the MDC for logging
				MdcUtil.clearMdc();
			}

//		};

//		executor.execute(executeProcess);

	}

}
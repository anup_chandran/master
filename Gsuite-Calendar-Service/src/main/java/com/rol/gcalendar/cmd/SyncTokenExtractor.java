package com.rol.gcalendar.cmd;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.rol.gcalendar.communicate.RestCall;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.exception.custom.GcalParseException;
import com.rol.gcalendar.vo.Integration;
import com.rol.gcalendar.vo.SharedResource;
import com.rol.gcalendar.vo.SubscribeRoomResponse;

@Component
@Scope("prototype")
public class SyncTokenExtractor {
	private GoogleCredential googleCredentials;
	@Autowired
	private HttpTransport transport;
	@Autowired
	private JacksonFactory jsonFactory;

	private String defaultUser;

	@Autowired
	@Qualifier("RestTemplateImpl")
	private RestCall restClient;
	
	@Autowired
	private ObjectMapper objectMapper ;
	
	private SubscribeRoomResponse subsRoomResp ;
	
	private SharedResource roomDb ;

	private Logger logger = LoggerFactory.getLogger(SyncTokenExtractor.class);

	public void extract(SubscribeRoomResponse subsRoomResp) throws GcalException, IOException {
		this.subsRoomResp = subsRoomResp;
		
		setDbParameters();
		setGCredentialObj();

		Calendar service = new Calendar.Builder(transport, jsonFactory, googleCredentials)
				.setApplicationName("GoogleSync").build();

		// Iterate over the events in the specified calendar
		String pageToken = Strings.EMPTY;
		do {
			Events events = service.events().list(roomDb.getGcalResourceEmail())
					.setPageToken(pageToken)
					.setSingleEvents(true)
					.execute();
			List<Event> items = events.getItems();
			for (Event event : items) {
				logger.info(event.getSummary());
			}
			pageToken = events.getNextPageToken();
			roomDb.setGsynctoken(events.getNextSyncToken());
		} while (pageToken != null);


		restClient.saveNextSyncToken(roomDb);

	}

	/**
	 * Sets the fresh instance of Google credentials with token
	 * 
	 * @throws GcalException
	 */
	
	private void setGCredentialObj() throws GcalException {
		googleCredentials = new GoogleCredential.Builder().setTransport(transport).setJsonFactory(jsonFactory).build();

		String	authResult = restClient.getRefreshAuthToken(defaultUser);

		Map<String, String> authMap = new HashMap<>();

		try {
			
			authMap = objectMapper.readValue(authResult, new TypeReference<Map<String, String>>() {
			});
		} catch (JsonProcessingException e) {
			throw getParseException(e, "Exception when getting the Auth Token details");
		}
		googleCredentials.setAccessToken(authMap.get("authToken"));
	}
	 

	private GcalParseException getParseException(Exception e, String message) {
		GcalParseException gCalParseExcp = new GcalParseException(message);
		if (e instanceof GoogleJsonResponseException) {
			GoogleJsonResponseException ge = (GoogleJsonResponseException) e;
			gCalParseExcp.getTokenErrorBean().setErrorCode(String.valueOf(ge.getStatusCode()));

		}
		gCalParseExcp.getTokenErrorBean().setErrorMessage(e.getMessage());
		return gCalParseExcp;
	}

	/**
	 * Set the default user by getting the details using the companyId
	 */
	private void setDbParameters() {
		roomDb = restClient.getRoomDb(subsRoomResp.getResourceId());
		Integration company =  restClient.getCompany(String.valueOf(roomDb.getCompanyid()));
		this.defaultUser = company.getDefaultUser() ;
	}

}

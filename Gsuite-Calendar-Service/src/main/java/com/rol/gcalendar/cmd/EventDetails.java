package com.rol.gcalendar.cmd;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.rol.gcalendar.communicate.RestCall;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.exception.custom.GcalParseException;
import com.rol.gcalendar.logcontext.MdcUtil;

/**
 * @author Anup
 *
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EventDetails {

	private String notifyJson;
	private String meetingDetailsJson;

	private String userName = "";
	private GoogleCredential googleCredentials;
	@Autowired
	private ObjectMapper objectMap;
	@Autowired
	private HttpTransport transport;
	@Autowired
	private JacksonFactory jsonFactory;
	@Autowired
	@Qualifier("RestTemplateImpl")
	private RestCall restClient;

	private Map<String, String> inJsonMap = new HashMap<>();

	private String calendarId = "";

	private boolean refreshAuthToken;

	/**
	 * @return
	 * @throws GcalParseException
	 */
	public String execute(String notifyJson) throws GcalException {
		this.notifyJson = notifyJson;

		parse();
		extractCalendar();
		extractUser();
		setGCredentialObj();
		extractMeetingDetails();

		return meetingDetailsJson;
	}
	
	private void extractUser() {
		userName = inJsonMap.get("id");
	}
	
	/**
	 * Parse the notification / watch string to extract the details
	 * 
	 * @param watchJson
	 * @throws GcalParseException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 */
	private void parse() throws GcalParseException {
		try {

			inJsonMap = objectMap.readValue(notifyJson, new TypeReference<Map<String, String>>() {
			});

		} catch (JsonProcessingException e) {
			throw getParseException(e, "Could not parse notification");
		}
	}

	/**
	 * Sets the fresh instance of Google credentials with token
	 * 
	 * @throws GcalException
	 */
	private void setGCredentialObj() throws GcalException {
		googleCredentials = new GoogleCredential.Builder().setTransport(transport).setJsonFactory(jsonFactory).build();

		// Set the token
		String authResult = "";
		if (refreshAuthToken) {
			authResult = restClient.getRefreshAuthToken(userName);
		} else {
			authResult = restClient.getAuthToken(userName);
		}

		Map<String, String> authMap = new HashMap<>();
		try {
			authMap = objectMap.readValue(authResult, new TypeReference<Map<String, String>>() {
			});
		} catch (JsonProcessingException e) {
			throw getParseException(e, "Exception when getting the Auth Token details");
		}
		googleCredentials.setAccessToken(authMap.get("authToken"));
	}

	/**
	 * @return
	 * @throws GcalException
	 */
	private void extractMeetingDetails() throws GcalException {
		try {
			Calendar calendar = new Calendar.Builder(transport, jsonFactory, googleCredentials)
					.setApplicationName("GoogleSync").build();
			
			Event eventDetails = calendar.events().get(calendarId, inJsonMap.get("resourceId")).execute();
			meetingDetailsJson = eventDetails.toString();
			MdcUtil.addSingleEntry("Meeting_Details", meetingDetailsJson);
		} catch (GoogleJsonResponseException gje) {
			if (HttpStatus.SC_UNAUTHORIZED == gje.getStatusCode()) {
				recoverUnAuthorizedError();
			}else {
				throw getParseException(gje, "Parse Exception when getting the meeting details , could not ");
			}
		} catch (IOException e) {
			throw getParseException(e, "Exception when getting the meeting details");
		}
	}

	/**
	 * @throws GcalException
	 */
	private void recoverUnAuthorizedError() throws GcalException {
		// we had tried getting the refresh Token ,by default refreshAuthToken is false !! exit
		if (refreshAuthToken) {
			throw getParseException(new Exception("Auth Token refresh error , exit !"),
					"Exception when getting the meeting details");
		}
		refreshAuthToken = true;
		// Reset the Token and call again
		setGCredentialObj();
		extractMeetingDetails();
	}

	/**
	 * @throws GcalParseException
	 * @throws URISyntaxException
	 * @throws GcalException
	 */
	private void extractCalendar() throws GcalParseException {

		try {
			URI uriConstruct = new URI(inJsonMap.get("resourceUri"));
			String tmp = "";
			String[] strArr = uriConstruct.getPath().split("/");

			for (int i = 0; i < strArr.length; i++) {
				tmp = strArr[i];
				if (tmp.contains("@")) {
					calendarId = tmp;
				}
			}

		} catch (URISyntaxException e) {
			throw getParseException(e, "Could not Extract user nmae from notification");
		}

		if (StringUtils.isEmpty(calendarId)) {
			throw getParseException(new Exception("UserName not Detected"),
					"Could not Extract user name from notification");
		}

	}

	private GcalParseException getParseException(Exception e, String message) {
		GcalParseException gCalParseExcp = new GcalParseException(message);
		if(e instanceof GoogleJsonResponseException) {
			GoogleJsonResponseException ge = (GoogleJsonResponseException)e ;
			gCalParseExcp.getTokenErrorBean().setErrorCode(String.valueOf(ge.getStatusCode()));
			
		}
		gCalParseExcp.getTokenErrorBean().setErrorMessage(e.getMessage());
		return gCalParseExcp;
	}

	public String getNotifyJson() {
		return notifyJson;
	}

	public void setNotifyJson(String notifyJson) {
		this.notifyJson = notifyJson;
	}

	public String getMeetingDetailsJson() {
		return meetingDetailsJson;
	}

	public void setMeetingDetailsJson(String meetingDetailsJson) {
		this.meetingDetailsJson = meetingDetailsJson;
	}

}

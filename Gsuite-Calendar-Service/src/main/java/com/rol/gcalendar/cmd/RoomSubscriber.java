package com.rol.gcalendar.cmd;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.rol.gcalendar.communicate.RestCall;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.vo.SubscribeRoomBean;
import com.rol.gcalendar.vo.SubscribeRoomReqBean;
import com.rol.gcalendar.vo.SubscribeRoomResponse;
import com.rol.gcalendar.vo.UnSubscribeRoomBean;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RoomSubscriber {

	@Value("${subscription_address}")
	private String webHookRecieveUrl;

	@Autowired
	@Qualifier("RestTemplateImpl")
	private RestCall restCall;

	@Autowired
	private SyncToken syncToken;

	/**
	 * @param subsBean
	 * @throws GcalException
	 */
	public SubscribeRoomResponse subscribe(final SubscribeRoomBean subsBean) throws GcalException {

		SubscribeRoomReqBean roomReqBn = new SubscribeRoomReqBean();
		roomReqBn.setId(UUID.randomUUID().toString());
		roomReqBn.setType("web_hook");
		roomReqBn.setAddress(webHookRecieveUrl);

		return restCall.subscribeRoom(roomReqBn, subsBean).join();

	}
	
	public void extractSyncToken(SubscribeRoomResponse subsRoomResp) {
		syncToken.process(subsRoomResp);
	}

	public void unSubscribe(UnSubscribeRoomBean unSubsBean) throws GcalException {
		restCall.unSubscribeRoom(unSubsBean);
	}

}

package com.rol.gcalendar.cmd;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.rol.gcalendar.communicate.RestCall;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.exception.custom.GcalParseException;
import com.rol.gcalendar.vo.Integration;
import com.rol.gcalendar.vo.SharedResource;

/**
 * @author Anup
 *
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EventsList {

	@Autowired
	@Qualifier("RestTemplateImpl")
	private RestCall restClient;

	private GoogleCredential googleCredentials;
	@Autowired
	private HttpTransport transport;
	@Autowired
	private JacksonFactory jsonFactory;

	@Autowired
	private ObjectMapper objectMapper;

	private SharedResource roomDb;
	private String gResourceId;
	private Integration company;

	Logger logger = LoggerFactory.getLogger(EventsList.class);

	/**
	 * @param gResourceId
	 * @return
	 * @throws GcalException
	 * @throws IOException
	 */
	public String extractMeetingLatestSyncToken(String gResourceId) throws GcalException, IOException {
		this.gResourceId = gResourceId;

		String result = Strings.EMPTY;

		getRoomDetails();
		setGCredentialObj(false);

		try {
			result = extractEvents();

		} catch (GoogleJsonResponseException excep) {// : 401 Unauthorized

			if (HttpStatus.SC_UNAUTHORIZED == excep.getStatusCode()) {
				setGCredentialObj(true);
				return extractEvents();
			}

			logger.error("Error Retrieving the List of meetings from Sync Token", excep);
		}

		logger.info(result);
		// Extract all the details
		return result;
	}

	private String extractEvents() throws IOException {
		String pageToken = Strings.EMPTY;
		Events events = null;
		List<Event> items = null;

		Calendar service = new Calendar.Builder(transport, jsonFactory, googleCredentials)
				.setApplicationName("GoogleSync").build();

		do {
			events = service.events().list(roomDb.getGcalResourceEmail())
					.setSingleEvents(true)
					.setSyncToken(roomDb.getGsynctoken())
					.setPageToken(pageToken).execute() ;
			items = events.getItems();
			for (Event event : items) {
				logger.info(event.getSummary());
			}
			pageToken = events.getNextPageToken();

			roomDb.setGsynctoken(events.getNextSyncToken());

		} while (pageToken != null);

		if (Objects.nonNull(items)) {
			// Save the next Sync Token
			restClient.saveNextSyncToken(roomDb);
			return items.toString();
		}

		return Strings.EMPTY;
	}

	private void getRoomDetails() {
		roomDb = restClient.getRoomDb(gResourceId);
		company = restClient.getCompany(String.valueOf(roomDb.getCompanyid()));
	}

	/**
	 * Sets the fresh instance of Google credentials with token
	 * 
	 * @throws GcalException
	 */

	private void setGCredentialObj(boolean refreshToken) throws GcalException {
		googleCredentials = new GoogleCredential.Builder().setTransport(transport).setJsonFactory(jsonFactory).build();

		String authResult = "";
		if (refreshToken) {
			authResult = restClient.getRefreshAuthToken(company.getDefaultUser());
		} else {
			authResult = restClient.getAuthToken(company.getDefaultUser());
		}

		Map<String, String> authMap = new HashMap<>();

		try {

			authMap = objectMapper.readValue(authResult, new TypeReference<Map<String, String>>() {
			});
		} catch (JsonProcessingException e) {
			throw getParseException(e, "Exception when getting the Auth Token details");
		}
		googleCredentials.setAccessToken(authMap.get("authToken"));
	}

	private GcalParseException getParseException(Exception e, String message) {
		GcalParseException gCalParseExcp = new GcalParseException(message);
		if (e instanceof GoogleJsonResponseException) {
			GoogleJsonResponseException ge = (GoogleJsonResponseException) e;
			gCalParseExcp.getTokenErrorBean().setErrorCode(String.valueOf(ge.getStatusCode()));

		}
		gCalParseExcp.getTokenErrorBean().setErrorMessage(e.getMessage());
		return gCalParseExcp;
	}

}

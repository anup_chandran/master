package com.rol.gcalendar.config;

import java.util.Collections;

import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.rol.gcalendar.interceptors.RestClientInterceptor;

@Component("RolRestTemplateConf")
public class RolRestTemplateConf implements RestTemplateCustomizer {

	@Override
	public void customize(RestTemplate restTemplate) {
		HttpComponentsClientHttpRequestFactory clientHttpReqFac = new HttpComponentsClientHttpRequestFactory();
		clientHttpReqFac.setReadTimeout(5000);
		clientHttpReqFac.setConnectTimeout(5000);
		clientHttpReqFac.setBufferRequestBody(true);
		
		ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(clientHttpReqFac);
		restTemplate.setRequestFactory(factory);
		
		restTemplate.setInterceptors(Collections.singletonList(new RestClientInterceptor()));
		

	}

}

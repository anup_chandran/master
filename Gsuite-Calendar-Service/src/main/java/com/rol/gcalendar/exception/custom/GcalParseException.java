package com.rol.gcalendar.exception.custom;

/**
 * @author Anup
 *
 */
public class GcalParseException extends GcalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5363583496236438920L;
	
	public GcalParseException(String errorMessage) {
		super.getTokenErrorBean().setErrorMessage(errorMessage);
	}

}

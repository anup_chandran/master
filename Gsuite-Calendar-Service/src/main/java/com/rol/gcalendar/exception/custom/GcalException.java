package com.rol.gcalendar.exception.custom;

import com.rol.gcalendar.vo.TokenErrorBean;

/**
 * @author Anup
 *
 */
public class GcalException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1217026581445748942L;
	private final transient TokenErrorBean tokenErrorBean = new TokenErrorBean() ;
	
	
	
	public TokenErrorBean getTokenErrorBean() {
		return this.tokenErrorBean ;
	}

}

package com.rol.gcalendar.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rol.gcalendar.cmd.RoomSubscriber;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.logcontext.MdcUtil;
import com.rol.gcalendar.service.MeetingService;
import com.rol.gcalendar.vo.SubscribeRoomBean;
import com.rol.gcalendar.vo.SubscribeRoomResponse;
import com.rol.gcalendar.vo.UnSubscribeRoomBean;

/**
 * @author Anup
 *
 */
@RestController
public class GsuiteCalendar {

	private static final Logger logger = LoggerFactory.getLogger(GsuiteCalendar.class);

	@Autowired
	@Qualifier("GoogleClient")
	private MeetingService meetingService;
	@Autowired
	private RoomSubscriber subscriber ;
	

	@PostMapping(value = "/meeting", produces = MediaType.APPLICATION_JSON_VALUE)
	public String meetingDetails(@RequestBody String injson) throws GcalException {
		try {
			return meetingService.getMeetingDetails(injson);
		} finally {
			logClear();
		}
	}
	
	@PostMapping(value = "/subscribe/room",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public SubscribeRoomResponse subscribeRoom(@RequestBody SubscribeRoomBean subsBean) throws GcalException {
		try {
			return subscriber.subscribe(subsBean);
		} finally {
			logClear();
		}
		
	}
	@PostMapping(value = "/unsubscribe/room",consumes = MediaType.APPLICATION_JSON_VALUE)
	public void unsubscribeRoom(@RequestBody UnSubscribeRoomBean unSubsBean) throws GcalException {
		subscriber.unSubscribe(unSubsBean);
	}
	@PostMapping(value = "/room/synctoken",consumes = MediaType.APPLICATION_JSON_VALUE)
	public void roomSyncToken(@RequestBody SubscribeRoomResponse subBnResp) throws GcalException {
		subscriber.extractSyncToken(subBnResp);
	}
	
	@GetMapping(value = "/room/updated/events/{gResourceId}",produces = MediaType.APPLICATION_JSON_VALUE)
	public String getEvents(@PathVariable String gResourceId) throws GcalException, IOException {
		return	meetingService.getEventsLatestSyncTkn(gResourceId) ;
	}
	
	
	private void logClear() {
		logger.info("Request/Respose Summary");
		MdcUtil.clearMdc();
	}

}

package com.rol.gcalendar.communicate;

import java.util.concurrent.CompletableFuture;

import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.vo.Integration;
import com.rol.gcalendar.vo.SharedResource;
import com.rol.gcalendar.vo.SubscribeRoomBean;
import com.rol.gcalendar.vo.SubscribeRoomReqBean;
import com.rol.gcalendar.vo.SubscribeRoomResponse;
import com.rol.gcalendar.vo.UnSubscribeRoomBean;

/**
 * @author Anup
 *
 */
public interface RestCall {
	
	/**
	 * Returns the Authentication token from the Auth service
	 * @param userName
	 * @return
	 */
	public String getAuthToken(String userName) throws GcalException ;
	
	/**
	 * @param userName
	 * @return
	 * @throws GcalException
	 */
	public String getRefreshAuthToken(String userName) throws GcalException ;
	
	/**
	 * @param roomReqBn
	 * @param subsRoomBn
	 * @return
	 * @throws GcalException
	 */
	public CompletableFuture<SubscribeRoomResponse> subscribeRoom(SubscribeRoomReqBean roomReqBn,SubscribeRoomBean subsRoomBn) throws GcalException ;
	
	/**
	 * @param roomReqBn
	 * @return
	 * @throws GcalException
	 */
	public void unSubscribeRoom(UnSubscribeRoomBean roomReqBn) throws GcalException ;
	
	/**
	 * @param userName
	 * @return
	 * @throws GcalException
	 */
//	public String extractToken(String userName) throws GcalException ;
	
	/**
	 * @param room
	 */
	public void saveNextSyncToken(SharedResource room) ;
	
	/**
	 * @param rolId
	 * @return
	 */
	public SharedResource getRoomDb(String gResourceId) ;
	
	/**
	 * @param cmdid
	 * @return
	 */
	public Integration getCompany(String cmdid) ;
	
	/**
	 * @param zone
	 */
	public void addRoom(SharedResource zone) ;

}

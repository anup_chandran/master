package com.rol.gcalendar.communicate;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rol.gcalendar.exception.custom.GcalException;
import com.rol.gcalendar.exception.custom.GcalParseException;
import com.rol.gcalendar.vo.Integration;
import com.rol.gcalendar.vo.ResourceMeetings;
import com.rol.gcalendar.vo.SharedResource;
import com.rol.gcalendar.vo.SubscribeRoomBean;
import com.rol.gcalendar.vo.SubscribeRoomReqBean;
import com.rol.gcalendar.vo.SubscribeRoomResponse;
import com.rol.gcalendar.vo.UnSubscribeRoomBean;

/**
 * @author Anup
 *
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Qualifier("RestTemplateImpl")
public class RestTemplateImpl implements RestCall {

	@Value("${auth_service_url}")
	private String authServiceUrl;

	@Value("${auth_service_refresh_url}")
	private String authServiceRefreshUrl;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${room_watch_url}")
	private String roomWatchUrl;// https://www.googleapis.com/calendar/v3/calendars/

	@Value("${room_stop_url}")
	private String webHookStopUrl;

	@Autowired
	private ObjectMapper objectMap;

	@Override
	public String getAuthToken(String userName) throws GcalException {
		return getTokenFromService(constructUrl(authServiceUrl, userName)).join();
	}

	@Override
	public String getRefreshAuthToken(String userName) throws GcalException {
		return getTokenFromService(constructUrl(authServiceRefreshUrl, userName)).join();
	}

	private Logger logger = LoggerFactory.getLogger(RestTemplateImpl.class);

	/**
	 * @param roomReqBn
	 * @param subsRoomBn
	 * @return
	 * @throws GcalException
	 */
	@Override
	@Async
	public CompletableFuture<SubscribeRoomResponse> subscribeRoom(SubscribeRoomReqBean roomReqBn, SubscribeRoomBean subsRoomBn)
			throws GcalException {
		roomWatchUrl = roomWatchUrl.replace("$$roomname$$", subsRoomBn.getGresourceid());

		ResponseEntity<SubscribeRoomResponse> respEntity = restTemplate.exchange(roomWatchUrl, HttpMethod.POST,
				getEntityMap(subsRoomBn.getDefaultuser(), roomReqBn), SubscribeRoomResponse.class);

		if (respEntity.getStatusCode().isError()) {
			throw new GcalParseException("Could not subscribe to Room " + roomReqBn.getAddress());
		}

		return CompletableFuture.completedFuture(respEntity.getBody());
	}

	@Override
	@Async
	public void unSubscribeRoom(UnSubscribeRoomBean roomReqBn) throws GcalException {
		ResponseEntity<String> respEntity = restTemplate.exchange(webHookStopUrl, HttpMethod.POST,
				getEntityMap(roomReqBn.getDefaultuser(), roomReqBn), String.class);

		if (respEntity.hasBody()) {
			logger.info(respEntity.getBody());
		}

		if (respEntity.getStatusCode().isError()) {
			throw new GcalParseException("Could not Stop Subscription  to Room " + roomReqBn.toString());
		}

	}
	
	public String extractToken(String userName) throws GcalException {
		String url = "https://www.googleapis.com/calendar/v3/calendars/$$resourceMail$$/events" ;
		url = url.replace("$$resourceMail$$", "evrygsuite.com_188511502j2mojf6mtk5li2gujob86g964s38d9o6sp38do@resource.calendar.google.com") ;
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		
		ResourceMeetings resourceMeetings = restTemplate.exchange(url, HttpMethod.GET, getEntityMap(userName), ResourceMeetings.class).getBody() ;
		//TODO repeat until we get the populate sync token
		
		return resourceMeetings.getNextSyncToken();
		
	}
	
	public void addRoom(SharedResource zone) {
		ResponseEntity<String> respEntity = restTemplate.postForEntity("http://localhost:8080/db/".concat("room/rolid/"),
				zone, String.class);
		if (respEntity.getStatusCode().isError()) {
			throw new RestClientException(
					"Could not Add Room in DB for ".concat(String.valueOf(zone.getGcalResourceEmail())));
		}

	}
	
	public void saveNextSyncToken(SharedResource room) {
		restTemplate.put("http://localhost:8080/db/room", room);
	}
	
	public SharedResource getRoomDb(String gResourceId) {
		ResponseEntity<SharedResource> respEntity = restTemplate.getForEntity("http://localhost:8080/db/room/gid/"+gResourceId, SharedResource.class);
		
		return respEntity.getBody() ; 
	}
	
	/**
	 * @param cmdid
	 * @return
	 */
	public Integration getCompany(String cmdid) {
		ResponseEntity<Integration> respEntity = restTemplate.getForEntity("http://localhost:8080/db/company/"+cmdid, Integration.class);
		
		return respEntity.getBody() ; 
	}

	
	/**
	 * @return
	 * @throws GcalException
	 */
	private <T> HttpEntity<T> getEntityMap(String userName, T element) throws GcalException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

		headers.add("Content-Type", "application/json");

		Map<String, String> authMap = new HashMap<>();
		//Always get a fresh token from 
		String authResult = getRefreshAuthToken(userName);

		try {
			authMap = objectMap.readValue(authResult, new TypeReference<Map<String, String>>() {
			});
		} catch (JsonProcessingException e) {
			throw new GcalParseException("Exception when getting the Auth Token details");
		}

		headers.add("Authorization", "Bearer ".concat(authMap.get("authToken")));

		return new HttpEntity<>(element, headers);

	}
	/**
	 * @return
	 * @throws GcalException
	 */
	private <T> HttpEntity<T> getEntityMap(String userName) throws GcalException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

		headers.add("Content-Type", "application/json");

		Map<String, String> authMap = new HashMap<>();
		//Always get a fresh token from 
		String authResult = getRefreshAuthToken(userName);

		try {
			authMap = objectMap.readValue(authResult, new TypeReference<Map<String, String>>() {
			});
		} catch (JsonProcessingException e) {
			throw new GcalParseException("Exception when getting the Auth Token details");
		}

		headers.add("Authorization", "Bearer ".concat(authMap.get("authToken")));

		return new HttpEntity<>(headers);

	}

	/**
	 * @param url
	 * @return
	 */
	@Async
	private CompletableFuture<String> getTokenFromService(String url) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		return CompletableFuture.completedFuture(restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class).getBody()) ;
	}

	/**
	 * @param baseUrl
	 * @param userName
	 * @return
	 * @throws GcalException
	 */
	private String constructUrl(String baseUrl, String userName) throws GcalException {
		try {
			URL url = new URL(baseUrl.concat(userName));
			return url.toString();
		} catch (MalformedURLException e) {
			throw new GcalParseException("Could not obtain Url");
		}
	}

}
